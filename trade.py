import math


class Trade():

    def __init__(self, data, account_balance, max_loss, stop_rate, limit_rate):
        self.data = data
        self.trade_record = []
        self.result_record = []
        self.counter_win = 0
        self.counter_lose = 0
        self.account_balance = account_balance
        self.start_account_balance = account_balance
        self.max_account_balance = 0
        self.max_drawdown = 0
        self.max_loss = max_loss
        self.lot = 0
        self.assets_recored = []
        self.total_lose_pip = 0
        self.total_win_pip = 0
        self.consecutive_losses_record = 0
        self.consecutive_winess_record = 0
        self.losses_counter = 0
        self.winess_counter = 0
        self.stop_rate = stop_rate
        self.limit_rate = limit_rate
    # ['date', 'open', 'high', 'low', 'close']

    def progress(self, i, data=None):
        if data is None:
            data = self.data
        if round(len(data) * 0.2) == i:
            print('進行度20%')
        if round(len(data) * 0.4) == i:
            print('進行度40%')
        if round(len(data) * 0.6) == i:
            print('進行度60%')
        if round(len(data) * 0.8) == i:
            print('進行度80%')

    def entry_buy(self, i, data=None, stop_rate=None, limit_rate=None):
        if data is None:
            data = self.data
        if stop_rate is None:
            stop_rate = self.stop_rate
        if limit_rate is None:
            limit_rate = self.limit_rate

        entry_buy = []
        entry_price = float(data['close'][i])
        rial_price = float(data['close'][i])
        stop_buy = round(float(data['low'][i]) - stop_rate, 3)  # stop buy
        stop_pip_buy = round((stop_buy - entry_price) * 100, 3)

        limit_pip_buy = round((stop_pip_buy / -100) *
                              limit_rate, 3)  # リミット、200で二倍

        limit_buy = round(entry_price + limit_pip_buy / 100, 3)  # limit buy
        state = 'buy'
        # 0:エントリー時間 1:エントリープライス 2:ストップ 3：リミット 4:ストップpip 5:リミットpip 6:state
        entry_buy.append([
            data['date'][i],  # entry time 0
            rial_price,  # entry price 1
            stop_buy,  # 2
            limit_buy,  # 3
            stop_pip_buy,  # 4
            limit_pip_buy,  # 5
            state,  # 6

        ])
        return entry_buy

    def entry_sell(self, i, data=None, stop_rate=None, limit_rate=None):
        if data is None:
            data = self.data
        if stop_rate is None:
            stop_rate = self.stop_rate
        if limit_rate is None:
            limit_rate = self.limit_rate

        entry_sell = []
        entry_price = float(data['close'][i])
        rial_price = float(data['close'][i])

        stop_sell = round(float(data['high'][i]) + stop_rate, 3)  # stop sell
        stop_pip_sell = round((entry_price - stop_sell) * 100, 3)
        limit_pip_sell = round((stop_pip_sell / -100) *
                               limit_rate, 3)  # リミット、200で二倍
        limit_sell = round(entry_price - limit_pip_sell / 100, 3)  # limit sell
        state = 'sell'
        # 0:エントリー時間 1:エントリープライス 2:ストップ 3：リミット 4:ストップpip 5:リミットpip 6:state
        entry_sell.append([
            data['date'][i],  # entry_sell time 0
            rial_price,  # entry_sell price 1
            stop_sell,  # 2
            limit_sell,  # 3
            stop_pip_sell,  # 4
            limit_pip_sell,  # 5
            state,  # 6

        ])
        return entry_sell

    def seve_trade(self, i, price, pip, data=None):
        if data is None:
            data = self.data
        self.result_record.append([
            self.trade_record[-1][0],  # entry time 0
            data['date'][i],  # end time 1
            self.trade_record[-1][1],  # 2
            self.trade_record[-1][price],  # 3
            self.trade_record[-1][pip],  # 4
            self.trade_record[-1][6],  # 5
        ])
        # result_record
        # 0:約定時間 1:決済時間 2:約定値段 3:決済値段 4:獲得pip 5:売買 6:結果

    def win(self, i):

        if self.trade_record[-1][6] == 'buy':
            self.seve_trade(i, 3, 5)
            self.result_record[-1].append('win')

        elif self.trade_record[-1][6] == 'sell':
            self.seve_trade(i, 3, 5)
            self.result_record[-1].append('win')

        self.trade_record[-1][6] = 'off'
        self.counter_win += 1

    def lose(self, i):

        if self.trade_record[-1][6] == 'buy':
            self.seve_trade(i, 2, 4)
            self.result_record[-1].append('lose')

        elif self.trade_record[-1][6] == 'sell':
            self.seve_trade(i, 2, 4)
            self.result_record[-1].append('lose')

        self.trade_record[-1][6] = 'off'
        self.counter_lose += 1

    def calculate(self):
        # ロット計算
        try:

            lot1 = self.account_balance * self.max_loss / \
                self.trade_record[-1][4] / 100
        except ZeroDivisionError:
            lot1 = self.account_balance * self.max_loss / 1000
        if lot1 <= 0:
            lot1 = lot1 * -1
        lot2 = self.account_balance * self.max_loss / 1000
        if lot1 >= lot2:
            self.lot = math.floor(lot2)
        else:
            self.lot = math.floor(lot1)

        # 損益
        profit = self.result_record[-1][4] * self.lot * 100
        self.account_balance = self.account_balance + profit
        rate_of_change = profit / self.account_balance * 100
        rate_of_change = rate_of_change

        # 勝率

        if self.counter_win > self.counter_lose:
            win_late = (self.counter_win /
                        (self.counter_win + self.counter_lose)) * 100

        else:
            win_late = (self.counter_win /
                        (self.counter_win + self.counter_lose)) * 100

        # リスクリワード
        if self.result_record[-1][6] == 'win':
            self.total_win_pip += self.result_record[-1][4]
        elif self.result_record[-1][6] == 'lose':
            self.total_lose_pip += self.result_record[-1][4] * -1
        self.total_lose_pip = self.total_lose_pip
        self.total_win_pip = self.total_win_pip
        try:

            risk_reward_ratio = (self.total_win_pip / self.counter_win) / \
                (self.total_lose_pip / self.counter_lose)
        except ZeroDivisionError:
            pass
        try:
            risk_reward_ratio = risk_reward_ratio
        except UnboundLocalError:
            risk_reward_ratio = 0
        ammount_get_pips = self.total_win_pip - self.total_lose_pip
        ammount_get_pips = ammount_get_pips

        # 最大連敗数
        if self.result_record[-1][6] == 'lose':
            self.losses_counter += 1
            if self.consecutive_losses_record < self.losses_counter:
                self.consecutive_losses_record = self.losses_counter
        else:
            self.losses_counter = 0
        # 最大連勝数
        if self.result_record[-1][6] == 'win':
            self.winess_counter += 1
            if self.consecutive_winess_record < self.winess_counter:
                self.consecutive_winess_record = self.winess_counter
        else:
            self.winess_counter = 0

        # ドローダウン
        drawdown = 0
        if self.max_account_balance <= self.account_balance:
            self.max_account_balance = self.account_balance

        else:
            drawdown = 100 - round(self.account_balance /
                                   self.max_account_balance * 100)
        if self.max_drawdown < drawdown:
            self.max_drawdown = drawdown

        # 期待値
        try:
            expected_value = self.total_win_pip / self.counter_win * win_late / \
                100 + self.total_lose_pip / \
                self.counter_lose * (win_late / 100 - 1)
        except ZeroDivisionError:
            expected_value = 0

        # 上昇率
        rate_of_up = self.account_balance / self.start_account_balance

        # 結果に追加
        self.result_record[-1].append(self.lot)  # ロット数
        self.result_record[-1].append(profit)  # 損益
        self.result_record[-1].append(round(rate_of_change, 2)),  # 損益率

        self.assets_recored.append([
            '口座残高:' + str(round(self.account_balance)),  # 口座残高
            '取引総数:' + str(self.counter_win + self.counter_lose),
            '勝率:' + str(round(win_late)),  # 勝率
            'リスクリワードレシオ:' + str(round(risk_reward_ratio, 2)),  # リスクリワードレシオ
            '連敗数:' + str(self.consecutive_losses_record),  # 連敗数
            '連勝数:' + str(self.consecutive_winess_record),  # 連勝数
            '合計ピップ数:' + str(round(ammount_get_pips)),  # 合計ピップ数
            'ドローダウン:' + str(self.max_drawdown),  # ドローダウン
            '期待値:' + str(round(expected_value, 2)),  # 期待値
            '上昇率:' + str(round(rate_of_up, 2))
        ])
