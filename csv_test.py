import indicater
import get_data
import trade_csv
import time
import csv
import random
import collections
import datetime

api = get_data.Api()

ex_piar = 'USD_JPY'
ex_piar_cut = ex_piar[3:]
span = 'M5'

ex_piar_15 = 'USD_JPY'
span_15 = 'M30'

ex_piar_3 = 'USD_JPY'
span_3 = 'H4'

time_zome = 'mt'

summer_time_list = []
if time_zome == 'mt':
    mt_time = 2
elif time_zome == 'jp':
    mt_time = 8

count = 0

#####################################################
maltitiime_trading = 0  # タイムフレームの数
if maltitiime_trading > 0:
    if span == 'M5':
        dt = datetime.timedelta(minutes=5)
    elif span == 'M15':
        dt = datetime.timedelta(minutes=15)
    elif span == 'M30':
        dt = datetime.timedelta(minutes=30)
    elif span == 'H1':
        dt = datetime.timedelta(hours=1)
    elif span == 'H4':
        dt = datetime.timedelta(hours=4)

test = 'on'
# 設定
if test == 'on':
    trading_style = 2  # 決済スタイル 1:ストップ変動なし 2:ストップ変動あり
    setting_stop_state = 3  # 1:ローソク足ベース 2:pipベース 3:priceベース
    setting_limit_state = 2  # 1:倍率200で二倍, 2:pipベース 3:priceベース
    limit_rate = 140  # 決済スタイル 1:リミット、200で二倍 2:pipベース
    stop_rate = 5  # 安値からのストップ幅
    yama_priod = 5  # 高値安値の判断に使う期間、トレールの位置も変わるので注意
    trail = 'on'  # onでトレーリングストップ
    flexible_settlement = 'o'  # 任意のタイミングで決済、!!trading_style == 2の時しか機能しない
    years_traid = 'n'  # onで年ごとのトレードになる、インデントに注意
    show = 'o'  # onでリアルタイム表示
    date_index = 0  # タイムフレーム2のインデックス
    date_index_2 = 0  # タイムフレーム3のインデックス
    spled = 0.4  # スプレット
    account_balance = 1000000  # 口座残高
    max_loss = 0.01  # 最大損失
    if ex_piar_cut == '_JPY':
        spled /= 100
        stop_rate /= 100
    else:
        spled /= 10000
        stop_rate /= 10000
    test_result_setting = {'trading_style': trading_style, 'setting_stop_state': setting_stop_state, 'setting_limit_state': setting_limit_state,
                           'limit_rate': limit_rate, 'stop_rate': stop_rate, 'yama_priod': yama_priod, 'trail': trail, 'maltitiime_trading': maltitiime_trading}

#####################################################
# エントリー条件
if test == 'on':
    condition = 0
    sell_condition = '''
if daw_another.trend_counter_list[-2] != daw_another.trend_counter_list[-1] and daw_another.trend_counter == -1:
    condition -= 1
    if maltitiime_trading >= 1 and daw_second.trend_counter <= -1:
        condition -= 1
        if maltitiime_trading >= 2 and  daw_third.trend_counter <= -1:
            condition -= 1
    '''
    buy_condition = '''
if daw_another.trend_counter_list[-2] != daw_another.trend_counter_list[-1] and daw_another.trend_counter == 1:
    condition += 1
    if maltitiime_trading >= 1 and daw_second.trend_counter >= 1:
        condition += 1
        if maltitiime_trading >= 2 and daw_third.trend_counter >= 1:
            condition += 1
    '''
# ****************************************************
# 決済の条件
settlement_condition = 0
judge_settlement_condition = '''
if ma5[0] > ma20[0]:
    settlement_condition = 1
'''
# ****************************************************
years = [
    # '2007',
    # '2008',
    # '2009',
    # '2010',
    # '2011',
    # '2012',
    # '2013',
    # '2014',
    # '2015',
    # '2016',
    # '2017',
    # '2018',
    '2019',
]

sum_all = []
sum_all_15 = []
sum_all_3 = []
for year in years:
    # 年単位の検証はここのインデントに合わせ、リストをコメントアウト
    if years_traid == 'on':
        sum_all = []
    if maltitiime_trading == 1 and years_traid == 'on':
        sum_all_15 = []
        sum_all_3 = []
    elif maltitiime_trading == 2 and years_traid == 'on':
        sum_all = []
        sum_all_15 = []
        sum_all_3 = []
    month = {}
    month['-01-01_'] = '-01-31.csv'
    # # うるう年
    if year == '2012' or year == '2016'or year == '2008':
        month['-02-01_'] = '-02-29.csv'
    else:
        month['-02-01_'] = '-02-28.csv'

    month['-03-01_'] = '-03-31.csv'
    month['-04-01_'] = '-04-30.csv'
    month['-05-01_'] = '-05-31.csv'
    month['-06-01_'] = '-06-30.csv'
    month['-07-01_'] = '-07-31.csv'
    month['-08-01_'] = '-08-31.csv'
    month['-09-01_'] = '-09-30.csv'
    month['-10-01_'] = '-10-31.csv'
    month['-11-01_'] = '-11-30.csv'
    month['-12-01_'] = '-12-31.csv'
    aa = indicater.summer_flag(year)
    summer_time_list.append(aa)
    for start, end in month.items():
        with open('data/' + ex_piar + '/' + year + '/' + span + '/' + year + start + year + end) as f:
            reader = csv.reader(f)
            data = [row for row in reader]
            del data[0]
            sum_all += data
    for start, end in month.items():
        with open('data/' + ex_piar_15 + '/' + year + '/' + span_15 + '/' + year + start + year + end) as f:
            reader = csv.reader(f)
            data = [row for row in reader]
            del data[0]
            sum_all_15 += data
    for start, end in month.items():
        with open('data/' + ex_piar_3 + '/' + year + '/' + span_3 + '/' + year + start + year + end) as f:
            reader = csv.reader(f)
            data = [row for row in reader]
            del data[0]
            sum_all_3 += data
# 全検証インデント
    # 年単位の検証インデント
i = 0
trade_pr = trade_csv.Trade(i, sum_all, account_balance,
                           max_loss, stop_rate, limit_rate, trading_style, setting_stop_state, setting_limit_state)
# インジケータークラス
if True:
    ma = indicater.Indicater()
# タイムフレーム1 dawクラス
if True:
    average = indicater.Indicater()
    daw = indicater.Daw()
    average_another = indicater.Indicater()
    daw_another = indicater.Daw()
    # タイムフレーム2
    average_second = indicater.Indicater()
    daw_second = indicater.Daw()
    # タイムフレーム3
    average_third = indicater.Indicater()
    daw_third = indicater.Daw()
    # ****************************************************

    print('test start' + str(len(sum_all)))
    start_time = time.time()
# date', 'open', 'high', 'low', 'close
for i in range(len(sum_all)):
    # プライスの代入
    trade_pr.i = i
    trade_pr.progress()  # 進行度の計測
    corrent_date = api.string_to_datetime(sum_all[i][0])
    corrent_price_open = float(sum_all[i][1])
    corrent_high_price = float(sum_all[i][2])
    corrent_low_price = float(sum_all[i][3])
    corrent_price = float(sum_all[i][4])
    time_difference = indicater.judge_summer_time(
        corrent_date, summer_time_list, mt_time)

# 直近高値安値の判別 priod=5  波動高値安の判別
    yama = average.get_high_and_low_yama(
        corrent_price, corrent_high_price, corrent_low_price, yama_priod)
    daw.judge_daw_state(yama, corrent_price)
    # daw.get_trendline_span(yama[0], yama[1], yama[2], i, sum_all)
    # daw_another.get_torendline_price(corrent_price, i, sum_all)

    yama_another = average_another.get_high_and_low_yama(
        corrent_price, corrent_high_price, corrent_low_price, 5)
    daw_another.judge_daw_state(yama_another, corrent_price)
    # daw_another.judge_patern(corrent_price)
    ma20 = ma.simple_moving_average(5, corrent_price)
    try:
        # if daw_another.trend_counter_list[-2] != daw_another.trend_counter_list[-1] and daw_another.trend_counter_list[-1] > 0:
        #     point_high = daw_another.latest_high_price_list[-1]
        #     count = 0
        # elif daw_another.trend_counter_list[-2] != daw_another.trend_counter_list[-1] and daw_another.trend_counter_list[-1] < 0:
        #     point_low = daw_another.latest_low_price_list[-1]
        #     count = 0
        print('----------------------')
        print(daw_another.trend_counter)
        print(yama)
        print(ma20[0])
        print(corrent_date + time_difference)
        
    except IndexError:
        pass

    # if daw_another.trend_counter_list[-1] > 0 and corrent_price < point_high:
    #     count += 1
    #     if count == 1:
    #         print(corrent_date + time_difference)
    # elif daw_another.trend_counter_list[-1] < 0 and corrent_price > point_low:
    #     count += 1
    #     if count == 1:
    #         print(corrent_date + time_difference)
    # if daw_another.trend_counter_list[-2] != daw_another.trend_counter_list[-1]:

    
    # トレール
    if trail == 'on':
        try:
            trade_pr.traling_stop(daw_another.latest_low_price_list[-1],
                                  daw_another.latest_high_price_list[-1],
                                  corrent_price,
                                  daw.trend_counter_list
                                  )
            # if daw.trend_counter_list[-2] != daw.trend_counter_list[-1]:
            #     print('------------------------')
            #     print(daw.latest_high_price_list[-1])
            #     print(daw.latest_low_price_list[-1])
        except IndexError:
            pass
# インジケーター
    # # yousen insen close で帰ってくる
    candle_line = average.judge_candle_state(
        corrent_price_open, corrent_price)
# マルチタイムフレーム
    if maltitiime_trading >= 1:
        try:
            if corrent_date == api.string_to_datetime(sum_all_15[date_index + 1][0]) - dt:
                corrent_high_price_no2 = float(sum_all_15[date_index][2])
                corrent_low_price_no2 = float(sum_all_15[date_index][3])
                yama_second = average_second.get_high_and_low_yama(
                    corrent_price, corrent_high_price_no2, corrent_low_price_no2, yama_priod)
                # 波動高値安の判別
                daw_second.judge_daw_state(
                    yama_second, corrent_price)
            if corrent_date > api.string_to_datetime(sum_all_15[date_index + 1][0]) - dt:
                date_index += 1

        except IndexError:
            pass
    if maltitiime_trading >= 2:
        try:
            if corrent_date == api.string_to_datetime(sum_all_3[date_index_2 + 1][0]) - dt:

                corrent_high_price_no3 = float(sum_all_3[date_index_2][2])
                corrent_low_price_no3 = float(sum_all_3[date_index_2][3])
                # 直近高値安値の判別
                yama_third = average_third.get_high_and_low_yama(
                    corrent_price, corrent_high_price_no3, corrent_low_price_no3, yama_priod)
                # 波動高値安の判別
                daw_third.judge_daw_state(
                    yama_third, corrent_price)
            if corrent_date > api.string_to_datetime(sum_all_3[date_index_2 + 1][0]) - dt:
                date_index_2 += 1
        except IndexError:
            pass
    daw.get_candle_counter(
        corrent_price, corrent_high_price, corrent_low_price)
# トレードの条件や設定
    try:
        exec(buy_condition)
        exec(sell_condition)
    except IndexError:
        pass
# 決済の条件
    if flexible_settlement == 'on':
        exec(judge_settlement_condition)
# 1:可変なしトレード 2:可変ありトレード
    if test == 'on':
        if i > 500 and trading_style == 1:
            try:
                # 負け条件
                if trade_pr.trade_record[-1][6] == 'sell' and trade_pr.trade_record[-1][2] < corrent_high_price + spled or trade_pr.trade_record[-1][6] == 'buy' and trade_pr.trade_record[-1][2] > corrent_low_price - spled:
                    if ex_piar_cut == '_JPY':
                        trade_pr.lose()
                    trade_pr.calculate()
                    if show == 'on':
                        print('--------------------------')
                        print('result:' + str(trade_pr.result_record[-1]))
                        print(trade_pr.assets_recored[-1])

            # 勝利条件
                elif trade_pr.trade_record[-1][6] == 'sell' and trade_pr.trade_record[-1][3] > corrent_low_price + spled or trade_pr.trade_record[-1][6] == 'buy' and trade_pr.trade_record[-1][3] < corrent_high_price - spled:
                    if ex_piar_cut == '_JPY':
                        trade_pr.win()
                    trade_pr.calculate()
                    if show == 'on':
                        print('--------------------------')
                        print('result:' + str(trade_pr.result_record[-1]))
                        print(trade_pr.assets_recored[-1])

            except IndexError:
                pass
            # 売り エントリー条件
            try:
                if condition == -1:
                    if not trade_pr.trade_record or trade_pr.trade_record[-1][6] == 'off':
                        if ex_piar_cut == '_JPY':
                            trade_pr.trade_record += trade_pr.entry_sell(
                                daw.high_price_list[-1],
                                daw.latest_high_price_list[-1],
                                daw.low_price_list[-1],
                                daw.latest_low_price_list[-1],

                            )
                        else:
                            trade_pr.trade_record += trade_pr.entry_sell_usd(i)
                        if show == 'on':
                            print('--------------------------')
                            print('entry:' + str(trade_pr.trade_record[-1]))
                    else:
                        pass

                    # 買い エントリー条件
                if condition == 1:
                    if not trade_pr.trade_record or trade_pr.trade_record[-1][6] == 'off':
                        if ex_piar_cut == '_JPY':
                            trade_pr.trade_record += trade_pr.entry_buy(
                                daw.low_price_list[-1],
                                daw.latest_low_price_list[-1],
                                daw.high_price_list[-1],
                                daw.latest_high_price_list[-1],

                            )
                        else:
                            trade_pr.trade_record += trade_pr.entry_buy_usd(i)
                        if show == 'on':
                            print('--------------------------')
                            print('entry:' + str(trade_pr.trade_record[-1]))
                    else:
                        pass
            except NameError:
                pass
            except AttributeError:
                pass
            except IndexError:
                pass

            # 可変トレード
        elif i > 500 and trading_style == 2:
            try:
                # 負け条件
                if trade_pr.trade_record[-1][6] == 'sell' and trade_pr.trade_record[-1][2] < corrent_high_price + spled or trade_pr.trade_record[-1][6] == 'buy' and trade_pr.trade_record[-1][2] > corrent_low_price - spled or settlement_condition == 1:
                    if ex_piar_cut == '_JPY':
                        trade_pr.lose()
                    trade_pr.calculate()
                    if show == 'on':
                        print('--------------------------')
                        print('result:' + str(trade_pr.result_record[-1]))
                        print(trade_pr.assets_recored[-1])

            # 勝利条件
                elif trade_pr.trade_record[-1][6] == 'sell' and trade_pr.trade_record[-1][3] > corrent_low_price + spled or trade_pr.trade_record[-1][6] == 'buy' and trade_pr.trade_record[-1][3] < corrent_high_price - spled:
                    if ex_piar_cut == '_JPY':
                        trade_pr.win()
                    else:
                        trade_pr.win_usd(i)
                    trade_pr.calculate()
                    if show == 'on':
                        print('--------------------------')
                        print('result:' + str(trade_pr.result_record[-1]))
                        print(trade_pr.assets_recored[-1])

            except IndexError:
                pass
            # 売り エントリー条件
            try:
                if condition == -1 and maltitiime_trading == 0 or condition == -2 and maltitiime_trading == 1 or condition == -3 and maltitiime_trading == 2:
                    if not trade_pr.trade_record or trade_pr.trade_record[-1][6] == 'off':
                        if ex_piar_cut == '_JPY':
                            trade_pr.trade_record += trade_pr.entry_sell(
                                daw_another.high_price_list,
                                daw.latest_high_price_list[-1],
                                daw.low_price_list,
                                daw.latest_low_price_list[-1],

                            )
                        else:
                            trade_pr.trade_record += trade_pr.entry_sell_usd(i)
                        if show == 'on':
                            print('--------------------------')
                            print('entry:' + str(trade_pr.trade_record[-1]))
                    else:
                        pass

                    # 買い エントリー条件
                if condition == 1 and maltitiime_trading == 0 or condition == 2 and maltitiime_trading == 1 or condition == 3 and maltitiime_trading == 2:
                    if not trade_pr.trade_record or trade_pr.trade_record[-1][6] == 'off':
                        if ex_piar_cut == '_JPY':
                            trade_pr.trade_record += trade_pr.entry_buy(
                                daw_another.low_price_list,
                                daw.latest_low_price_list[-1],
                                daw.high_price_list[-1],
                                daw.latest_high_price_list[-1],

                            )
                        else:
                            trade_pr.trade_record += trade_pr.entry_buy_usd(i)
                        if show == 'on':
                            print('--------------------------')
                            print('entry:' + str(trade_pr.trade_record[-1]))
                    else:
                        pass
            except NameError:
                pass
            except AttributeError:
                pass
            except IndexError:
                pass
        condition = 0
        settlement_condition = 0
if test == 'on':
    end_time = time.time()
    take_time = round((end_time - start_time) / 60, 2)
    print('テストタイム' + str(take_time))
    print(trade_pr.counter_lose)
    print(trade_pr.counter_win)
    print(trade_pr.counter_win + trade_pr.counter_lose)
    print(trade_pr.trade_record[-1])
    print('--------------------------')
    print(trade_pr.result_record[-1])
    print(trade_pr.assets_recored[-1])
    print(buy_condition)
    print(sell_condition)
    print(test_result_setting)
