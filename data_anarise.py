import indicater
import get_data
import trade_csv
import time
import csv
import random
import collections
import datetime

api = get_data.Api()
api_2 = get_data.Api()

ex_piar = 'USD_JPY'
ex_piar_cut = ex_piar[3:]
span = 'M5'

# マルチタイムフレーム設定
ex_piar_2 = 'USD_JPY'
ex_piar_cut_2 = ex_piar[3:]
span_2 = 'H1'

ex_piar_3 = 'USD_JPY'
ex_piar_cut_3 = ex_piar[3:]
span_3 = 'D'

if span == 'M5':
    dt = datetime.timedelta(minutes=5)
# 設定
maltitiime_trading = 0  # タイムフレームの数
date_index = 0  # タイムフレーム2のインデックス
date_index_2 = 0  # タイムフレーム3のインデックス
yama_priod = 20  # 高値安値の判断に使う期間、トレールの位置も変わるので注意
years = [
    # '2010',
    # '2011',
    # '2012',
    # '2013',
    # '2014',
    # '2015',
    # '2016',
    # '2017',
    # '2018',
    '2019',
]

sum_all = []
sum_all_2 = []
sum_all_3 = []
for year in years:
    # 年単位の検証はここのインデントに合わせ、リストをコメントアウト
    # sum_all = []
    # sum_all_2 = []
    month = {}
    month['-01-01_'] = '-01-31.csv'
    # # うるう年
    if year == '2012' or year == '2016':
        month['-02-01_'] = '-02-29.csv'
    else:
        month['-02-01_'] = '-02-28.csv'

    month['-03-01_'] = '-03-31.csv'
    month['-04-01_'] = '-04-30.csv'
    month['-05-01_'] = '-05-31.csv'
    month['-06-01_'] = '-06-30.csv'
    month['-07-01_'] = '-07-31.csv'
    month['-08-01_'] = '-08-31.csv'
    month['-09-01_'] = '-09-30.csv'
    month['-10-01_'] = '-10-31.csv'
    month['-11-01_'] = '-11-30.csv'
    month['-12-01_'] = '-12-31.csv'
    for start, end in month.items():
        with open('data/' + ex_piar + '/' + year + '/' + span + '/' + year + start + year + end) as f:
            reader = csv.reader(f)
            data = [row for row in reader]
            del data[0]
            sum_all += data
    for start, end in month.items():
        with open('data/' + ex_piar_2 + '/' + year + '/' + span_2 + '/' + year + start + year + end) as f:
            reader = csv.reader(f)
            data = [row for row in reader]
            del data[0]
            sum_all_2 += data
    for start, end in month.items():
        with open('data/' + ex_piar_3 + '/' + year + '/' + span_3 + '/' + year + start + year + end) as f:
            reader = csv.reader(f)
            data = [row for row in reader]
            del data[0]
            sum_all_3 += data
# 全検証インデント
    # 年単位の検証インデント
    # 設定
# インジケーター
average = indicater.Indicater()
daw = indicater.Daw()
print('test start' + str(len(sum_all)))
print('test start' + str(len(sum_all_2)))
print('test start' + str(len(sum_all_3)))
start_time = time.time()

# date', 'open', 'high', 'low', 'close
# ブレイク 高値安値のブレイク 以降無限売買

for i in range(len(sum_all)):
    corrent_date = api.string_to_datetime(sum_all[i][0])
    corrent_price_open = float(sum_all[i][1])
    corrent_high_price = float(sum_all[i][2])
    corrent_low_price = float(sum_all[i][3])
    corrent_price = float(sum_all[i][4])

    daw.get_candle_counter(
        corrent_price, corrent_high_price, corrent_low_price)
    try:
        # ダウで状態確認する関数
        yama_list = average.get_high_and_low_yama(
            corrent_price, corrent_high_price, corrent_low_price, yama_priod)
        daw.judge_daw_state(yama_list, corrent_price)

    except IndexError:
        pass
    except NameError:
        pass
    # マルチタイムフレーム
    if maltitiime_trading >= 1:
        try:
            for k in range(date_index, date_index + 30):
                if corrent_date == api.string_to_datetime(sum_all_2[k + 1][0]) - dt:
                    # print(sum_all_2[k][0])
                    corrent_high_price_no2 = float(sum_all_2[k][2])
                    corrent_low_price_no2 = float(sum_all_2[k][3])

                    date_index = k
                    break
                else:
                    pass
        except IndexError:
            pass
    if maltitiime_trading >= 2:
        try:
            for p in range(date_index_2, date_index_2 + 30):
                if corrent_date == api.string_to_datetime(sum_all_3[k + 1][0]) - dt:
                    # print(sum_all_3[k][0])
                    corrent_high_price_no2 = float(sum_all_3[k][2])
                    corrent_low_price_no2 = float(sum_all_3[k][3])

                    date_index_2 = p
                    break
                else:
                    pass
        except IndexError:
            pass
    try:
        if daw.trend_counter_list[-1] != daw.trend_counter_list[-2]:
            print('------------------------')
            print(corrent_date)
            print(daw.trend_counter)
            print(daw.trend_state)
            print(daw.latest_high_price_list[-1])
            print(corrent_price)
            print(daw.latest_low_price_list[-1])
    except IndexError:
        pass
