import indicater
import get_data
import trade
import time

print('start get data')
api = get_data.Api()
# データ

# data1 = api.price("2019-06-30T19:11:59", "2019-01-01T19:12:00")
# data2 = api.price("2019-12-31T19:11:59", "2019-07-01T19:12:00")
# data3 = api.price("2018-06-30T19:11:59", "2018-01-01T19:12:00")
# data4 = api.price("2018-12-31T19:11:59", "2018-07-01T19:12:00")
# data5 = api.price("2017-06-30T19:11:59", "2017-01-01T19:12:00")
# data6 = api.price("2017-12-31T19:11:59", "2017-07-01T19:12:00")
# data7 = api.price("2016-06-30T19:11:59", "2016-01-01T19:12:00")
# data8 = api.price("2016-12-31T19:11:59", "2016-07-01T19:12:00")
# data9 = api.price("2015-06-30T19:11:59", "2015-01-01T19:12:00")
# data10 = api.price("2015-12-31T19:11:59", "2015-07-01T19:12:00")
# data11 = api.price("2014-06-30T19:11:59", "2014-01-01T19:12:00")
# data12 = api.price("2014-12-31T19:11:59", "2014-07-01T19:12:00")
# data13 = api.price("2013-06-30T19:11:59", "2013-01-01T19:12:00")
# data14 = api.price("2013-12-31T19:11:59", "2013-07-01T19:12:00")
# data15 = api.price("2012-06-30T19:11:59", "2012-01-01T19:12:00")
# data16 = api.price("2012-12-31T19:11:59", "2012-07-01T19:12:00")
# sum2019 = data1 + data2
# sum2018 = data3 + data4
# sum2017 = data5 + data6
# sum2016 = data7 + data8
# sum2015 = data9 + data10
# sum2014 = data11 + data12
# sum2013 = data13 + data14
# sum2012 = data15 + data16
# sum_all = sum2012 + sum2013 + sum2014 + \
#     sum2015 + sum2016 + sum2017 + sum2018 + sum2019

# 一か月ベースの検証
data_list_year = [
    '2010',
    '2011',
    '2012',
    '2013',
    '2014',
    '2015',
    '2016',
    '2017',
    '2018',
    '2019'
]
for year in data_list_year:
    print('---------------' + year + '-----------------')
    data_dict = {
        year + '-01-31T19:11:59': year + '-01-01T19:12:00',
        year + '-02-28T19:11:59': year + '-02-01T19:12:00',
        year + '-03-31T19:11:59': year + '-03-01T19:12:00',
        year + '-04-30T19:11:59': year + '-04-01T19:12:00',
        year + '-05-31T19:11:59': year + '-05-01T19:12:00',
        year + '-06-30T19:11:59': year + '-06-01T19:11:59',
        year + '-07-31T19:11:59': year + '-07-01T19:11:59',
        year + '-08-31T19:11:59': year + '-08-01T19:11:59',
        year + '-09-30T19:11:59': year + '-09-01T19:11:59',
        year + '-10-31T19:11:59': year + '-10-01T19:11:59',
        year + '-11-30T19:11:59': year + '-11-01T19:11:59',
        year + '-12-31T19:11:59': year + '-12-01T19:12:00'
    }

    for end, start in data_dict.items():
        # 一か月ベースの検証
        sum_all = api.price(end, start)
        data = api.save(sum_all)
        print('get' + str(len(data)))

        # 設定
        spled = 0.008  # スプレット
        account_balance = 300000  # 口座残高
        max_loss = 0.01  # 最大損失
        stop_rate = 0.02  # 安値からのストップ幅
        limit_rate = 200  # リミット、200で二倍
        trade_pr = trade.Trade(data, account_balance,
                            max_loss, stop_rate, limit_rate)
        # インジケーター

        period_1 = 5
        average = indicater.Indicater()
        ma5 = indicater.Indicater()
        ma20 = indicater.Indicater()
        print('test start')
        start_time = time.time()
        # ブレイク 高値安値のブレイク 以降無限売買
        for i in range(len(data)):
            trade_pr.progress(i)  # 進行度の計測
            no = ''
            corrent_price = float(data['close'][i])
            corrent_high_price = float(data['high'][i])
            corrent_low_price = float(data['low'][i])
            ma = average.set_yama(corrent_price)
            be = average.get_yama(ma, average.rate_3)
            ma_5 = ma5.simple_moving_average(5, corrent_price)
            ma_20 = ma20.simple_moving_average(20, corrent_price)
            
            try:
                if 'low' in be[-1]:
                    low = be[-1]['low']
                elif 'high' in be[-1]:
                    high = be[-1]['high']
                else:
                    no = be[-1]
            except:
                pass
            if i > 100:
                # 売り
                if no == 'no':
                    if corrent_price < low and corrent_price < ma_5[0] < ma_20[0]:

                        if not trade_pr.trade_record or trade_pr.trade_record[-1][6] == 'off':
                            trade_pr.trade_record += trade_pr.entry_sell(i)
                        else:
                            pass
                            if trade_pr.trade_record[-1][6] == 'sell' and trade_pr.trade_record[-1][2] < corrent_high_price + spled:
                                trade_pr.lose(i)
                                trade_pr.calculate()
                            elif trade_pr.trade_record[-1][6] == 'sell' and trade_pr.trade_record[-1][3] > corrent_low_price + spled:
                                trade_pr.win(i)
                                trade_pr.calculate()
                        # 買い
                    elif corrent_price > high and corrent_price > ma_5[0] > ma_20[0]:
                        if not trade_pr.trade_record or trade_pr.trade_record[-1][6] == 'off':
                            trade_pr.trade_record += trade_pr.entry_buy(i)
                        else:
                            pass
                            if trade_pr.trade_record[-1][6] == 'buy' and trade_pr.trade_record[-1][2] > corrent_low_price - spled:
                                trade_pr.lose(i)
                                trade_pr.calculate()
                            elif trade_pr.trade_record[-1][6] == 'buy' and trade_pr.trade_record[-1][3] < corrent_high_price - spled:
                                trade_pr.win(i)
                                trade_pr.calculate()
            

end_time = time.time()
take_time = (end_time - start_time) / 60
print('テストタイム' + str(take_time))
print(trade_pr.counter_lose)
print(trade_pr.counter_win)
print(trade_pr.counter_win + trade_pr.counter_lose)
print(trade_pr.result_record[-1])
print(trade_pr.assets_recored[-1])
