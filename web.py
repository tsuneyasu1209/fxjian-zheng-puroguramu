import sys
import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import user
user_id = user.user_id
password = user.password


class Web(object):
    browser = webdriver.Chrome('chromedriver.exe')

    def __init__(self, user_id, password):
        self.user_id = user_id
        self.password = password

    def userLogin(self):
        time.sleep(5)
        self.browser.get('https://www.oanda.jp/trade/practice/login.do')

        # cur_url = browser.current_url
        # if 'self' in cur_url:
        # 要素の指定
        elem_id = self.browser.find_element_by_xpath(
            '//*[@id="practice_id"]')
        # 実際の打ち込み
        elem_id.send_keys(self.user_id)
        elem_userpassword = self.browser.find_element_by_xpath(
            '//*[@id="password-active"]')
        elem_userpassword.send_keys(self.password)

        elem_login_btn = self.browser.find_element_by_xpath(
            '//*[@id="login_submit"]')
        elem_login_btn.click()
        time.sleep(5)

        elem_trade_btn = self.browser.find_element_by_xpath(
            '//*[@id="Container"]/div[2]/div/div[1]/div[2]/a')

        elem_trade_btn.click()
        time.sleep(10)

    def get_usd_jpy_orderpage(self):
        orderpage = self.browser.find_element_by_xpath(
            '//*[@id="ratesComponent"]/div/rates-panel-selector/div/div[2]/div/iron-pages/panel-view/div/panel-view-quote[4]/div/div[2]/button[2]')
        orderpage.click()

        clear_lot = clear_lot = self.browser.find_element_by_xpath(
            '//*[@id="unit-input"]/div/input')
        clear_lot.clear()

    def position_info(self):
        position_check1 = self.browser.find_element_by_xpath(
            '//*[@id="mainContainer"]/account-summary/button/div[5]/div[2]').get_attribute("textContent")

        position_check2 = self.browser.find_element_by_xpath(
            '//*[@id="mainContainer"]/account-summary/button/div[10]/div[2]').get_attribute("textContent")
        if position_check1 == position_check2:
            position = True
        else:
            position = False
        return position

    def get_spled_and_balance(self):
        spled = self.browser.find_element_by_xpath(
            '//*[@id="spreadBox"]').get_attribute("textContent")

        balance = self.browser.find_element_by_xpath(
            '//*[@id="pages"]/market-view/views-template/div[1]/div[2]/div[6]').get_attribute("textContent")
        balance = balance.replace(",", "")
        balance = balance[:-7]

        return {'spled': float(spled), 'balance': int(balance)}

    def order(self, lot, stop_rate, limit_rate, state):
        if state == 'buy':
            select_buy = self.browser.find_element_by_xpath(
                '//*[@id="buyBox"]')
            select_buy.click()
        elif state == 'sell':
            select_sell = self.browser.find_element_by_xpath(
                '//*[@id="sellBox"]')
            select_sell.click()

        input_lot = self.browser.find_element_by_xpath(
            '//*[@id="unit-input"]/div/input')

        close_btn = self.browser.find_element_by_xpath(
            '//*[@id="orderDialog"]/div/div[1]/img')

        input_stop = self.browser.find_element_by_xpath(
            '//*[@id="pages"]/market-view/views-template/div[3]/oanda-collapsable-field[2]/div[2]/oanda-pricing-field/div/oanda-number-input[1]/div/input')
        input_limit = self.browser.find_element_by_xpath(
            '//*[@id="pages"]/market-view/views-template/div[3]/oanda-collapsable-field[1]/div[2]/oanda-pricing-field/div/oanda-number-input[1]/div/input')

        order_btn = self.browser.find_element_by_xpath(
            '//*[@id="submit"]')

        input_lot.send_keys(lot)
        input_stop.send_keys(stop_rate)
        input_limit.send_keys(limit_rate)
        order_btn.click()
        try:
            close_btn.click()
        except:
            pass
        
