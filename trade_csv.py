import math


class Trade():

    def __init__(self, i, data, account_balance, max_loss, stop_rate, limit_rate, trading_style, setting_stop_state, setting_limit_state):
        self.i = i
        self.data = data
        self.trade_record = []
        self.result_record = []
        self.counter_win = 0
        self.counter_lose = 0
        self.account_balance = account_balance
        self.start_account_balance = account_balance
        self.max_account_balance = 0
        self.max_drawdown = 0
        self.max_loss = max_loss
        self.lot = 0
        self.assets_recored = []
        self.total_lose_pip = 0
        self.total_win_pip = 0
        self.consecutive_losses_record = 0
        self.consecutive_winess_record = 0
        self.losses_counter = 0
        self.winess_counter = 0
        self.stop_rate = stop_rate
        self.limit_rate = limit_rate
        self.candle_data = []
        self.trading_style = trading_style
        self.trail_price_list = []
        self.trail_pip_list = []
        self.calculate_pip_list = []
        self.setting_stop_state = setting_stop_state
        self.setting_limit_state = setting_limit_state

    # ['date', 'open', 'high', 'low', 'close']

    def progress(self):
        if round(len(self.data) * 0.2) == self.i:
            print('進行度20%')
        if round(len(self.data) * 0.4) == self.i:
            print('進行度40%')
        if round(len(self.data) * 0.6) == self.i:
            print('進行度60%')
        if round(len(self.data) * 0.8) == self.i:
            print('進行度80%')

    def entry_buy(self, latest_low, latest_max_low, latest_high, latest_max_high):
        entry_buy = []
        entry_price = float(self.data[self.i][4])
        rial_price = float(self.data[self.i][4])
        state = 'buy'
        if self.setting_stop_state == 1:  # ローソク足
            stop_buy = round(
                float(self.data[self.i][3]) - self.stop_rate, 3)  # stop buy
            stop_pip_buy = round((stop_buy - entry_price) * 100, 3)
        elif self.setting_stop_state == 2:  # pipベース
            stop_pip_buy = self.stop_rate * -100
            stop_buy = round(entry_price + stop_pip_buy / 100, 3)
        elif self.setting_stop_state == 3:  # priceベース
            for stop_number in reversed(range(len(latest_low))):
                stop_buy = round(float(latest_low[stop_number]) -
                                 self.stop_rate, 3)  # stop buy
                stop_pip_buy = round((stop_buy - entry_price) * 100, 3)
                if stop_pip_buy < 0:
                    break

        if self.setting_limit_state == 1:  # 倍率
            limit_pip_buy = round((stop_pip_buy / -100) *
                                  self.limit_rate, 3)  # リミット、200で二倍
            limit_buy = round(entry_price + limit_pip_buy /
                              100, 3)  # limit buy
        elif self.setting_limit_state == 2:  # pip
            limit_pip_buy = self.limit_rate
            limit_buy = round(entry_price + limit_pip_buy / 100, 3)
        elif self.setting_limit_state == 3:  # price
            limit_buy = latest_high
            limit_pip_buy = round((entry_price - limit_buy) * -100)

        entry_buy.append([
            self.data[self.i][0],  # entry time 0
            rial_price,  # entry price 1
            stop_buy,  # 2
            limit_buy,  # 3
            stop_pip_buy,  # 4
            limit_pip_buy,  # 5
            state,  # 6
        ])
        self.trail_pip_list.append(entry_buy[-1][4])
        self.calculate_pip_list.append(entry_buy[-1][4])
        return entry_buy
        # 0:エントリー時間 1:エントリープライス 2:ストップ 3：リミット 4:ストップpip 5:リミットpip 6:state

    def entry_sell(self, latest_high, latest_max_high, latest_low, latest_max_low):
        entry_sell = []
        entry_price = float(self.data[self.i][4])
        rial_price = float(self.data[self.i][4])
        state = 'sell'
        if self.setting_stop_state == 1:  # ローソク足
            stop_sell = round(
                float(self.data[self.i][2]) + self.stop_rate, 3)  # stop sell
            stop_pip_sell = round((entry_price - stop_sell) * 100, 3)
        elif self.setting_stop_state == 2:  # pipベース
            stop_pip_sell = self.stop_rate * -100
            stop_sell = round(entry_price + stop_pip_sell / -100, 3)
        elif self.setting_stop_state == 3:  # priceベース
            for stop_number in reversed(range(len(latest_high))):
                stop_sell = round(float(latest_high[stop_number]) +
                                  self.stop_rate, 3)  # stop sell
                stop_pip_sell = round((entry_price - stop_sell) * 100, 3)
                if stop_pip_sell < 0:
                    break

        if self.setting_limit_state == 1:  # 倍率
            limit_pip_sell = round((stop_pip_sell / -100) *
                                   self.limit_rate, 3)  # リミット、200で二倍
            limit_sell = round(
                entry_price - limit_pip_sell / 100, 3)  # limit sell
        elif self.setting_limit_state == 2:  # pipベース
            limit_pip_sell = self.limit_rate
            limit_sell = round(
                entry_price - limit_pip_sell / 100, 3)
        elif self.setting_limit_state == 3:  # priceベース
            for limit_number in reversed(range(len(latest_low))):
                limit_sell = latest_low[limit_number]
                limit_pip_sell = round(
                    ((entry_price - limit_sell) * 100), 1)
                if limit_pip_sell > 0:
                    limit_sell = latest_low[limit_number] - \
                        self.limit_rate / 100
                    limit_pip_sell = round((entry_price - limit_sell) * 100, 1)
                    break

        # 0:エントリー時間 1:エントリープライス 2:ストップ 3：リミット 4:ストップpip 5:リミットpip 6:state
        entry_sell.append([
            self.data[self.i][0],  # entry_sell time 0
            rial_price,  # entry_sell price 1
            stop_sell,  # 2
            limit_sell,  # 3
            stop_pip_sell,  # 4
            limit_pip_sell,  # 5
            state,  # 6
        ])
        self.trail_pip_list.append(entry_sell[-1][4])
        self.calculate_pip_list.append(entry_sell[-1][4])
        return entry_sell

    def traling_stop(self, low_price, high_price, corrent_price, trend_counter_list):
        if self.trade_record[-1][6] == 'buy':
            if trend_counter_list[-2] != trend_counter_list[-1]:
                corrent_pips = round(
                    (self.trade_record[-1][1] - (low_price - self.stop_rate)) * -100, 1)
                if self.calculate_pip_list[-1] < corrent_pips:
                    self.calculate_pip_list.append(corrent_pips)
                    self.trade_record[-1][2] = round(
                        low_price - self.stop_rate, 3)
                    self.trade_record[-1][4] = round((self.trade_record[-1]
                                                      [1] - self.trade_record[-1][2]) * -100, 1)
                    self.trail_price_list.append(self.trade_record[-1][2])
                    # print('+++++++++トレール買い++++++++++++++++')
                    # print(self.data[self.i][0])
                    # print(self.trade_record[-1][2])
                    # print(self.trade_record[-1][4])

        elif self.trade_record[-1][6] == 'sell':
            if trend_counter_list[-2] != trend_counter_list[-1]:
                corrent_pips = round((self.trade_record[-1]
                                      [1] - (high_price + self.stop_rate)) * 100, 1)
                if self.calculate_pip_list[-1] < corrent_pips:
                    self.calculate_pip_list.append(corrent_pips)
                    self.trade_record[-1][2] = round(
                        high_price + self.stop_rate, 3)
                    self.trade_record[-1][4] = round((self.trade_record[-1]
                                                      [1] - self.trade_record[-1][2]) * 100, 1)
                    self.trail_price_list.append(self.trade_record[-1][2])
                    # print('------------トレール売り-------------')
                    # print(self.data[self.i][0])
                    # print(self.trade_record[-1][2])
                    # print(self.trade_record[-1][4])

    def seve_trade(self, price, pip):
        self.result_record.append([
            self.trade_record[-1][0],  # entry time 0
            self.data[self.i][0],  # end time 1
            self.trade_record[-1][1],  # 2
            self.trade_record[-1][price],  # 3
            self.trade_record[-1][pip],  # 4
            self.trade_record[-1][6],  # 5
        ])

        # result_record
        # 0:約定時間 1:決済時間 2:約定値段 3:決済値段 4:獲得pip 5:売買 6:結果

    def win(self):
        if self.trade_record[-1][6] == 'buy':
            self.seve_trade(3, 5)
            self.result_record[-1].append('win')

        elif self.trade_record[-1][6] == 'sell':
            self.seve_trade(3, 5)
            self.result_record[-1].append('win')

        self.trade_record[-1][6] = 'off'
        self.counter_win += 1

    def lose(self):
        if self.trade_record[-1][4] <= 0:
            if self.trade_record[-1][6] == 'buy':
                self.seve_trade(2, 4)
                self.result_record[-1].append('lose')

            elif self.trade_record[-1][6] == 'sell':
                self.seve_trade(2, 4)
                self.result_record[-1].append('lose')
            self.counter_lose += 1

        elif self.trade_record[-1][4] > 0:
            if self.trade_record[-1][6] == 'buy':
                self.seve_trade(2, 4)
                self.result_record[-1].append('win')

            elif self.trade_record[-1][6] == 'sell':
                self.seve_trade(2, 4)
                self.result_record[-1].append('win')
            self.counter_win += 1
        self.trade_record[-1][6] = 'off'

    def calculate(self):
        # ロット計算
        try:
            if len(self.trail_pip_list) == 0:
                lot1 = self.account_balance * self.max_loss / \
                    self.trade_record[-1][4] / 100
            else:
                lot1 = self.account_balance * self.max_loss / \
                    self.trail_pip_list[-1] / 100
        except ZeroDivisionError:
            lot1 = self.account_balance * self.max_loss / 1000
        if lot1 <= 0:
            lot1 = lot1 * -1
        lot2 = self.account_balance * self.max_loss / 1000
        if lot1 >= lot2:
            self.lot = math.floor(lot2)
        else:
            self.lot = math.floor(lot1)

        # 勝率
        if self.counter_win > self.counter_lose:
            win_late = (self.counter_win /
                        (self.counter_win + self.counter_lose)) * 100

        else:
            win_late = (self.counter_win /
                        (self.counter_win + self.counter_lose)) * 100

        # リスクリワード
        if self.result_record[-1][6] == 'win':
            self.total_win_pip += self.result_record[-1][4]
        elif self.result_record[-1][6] == 'lose':
            self.total_lose_pip += self.result_record[-1][4]
        self.total_lose_pip = self.total_lose_pip
        self.total_win_pip = self.total_win_pip
        try:

            risk_reward_ratio = (self.total_win_pip / self.counter_win) / \
                (self.total_lose_pip / self.counter_lose)
        except ZeroDivisionError:
            pass
        try:
            risk_reward_ratio = risk_reward_ratio
        except UnboundLocalError:
            risk_reward_ratio = 0
        ammount_get_pips = self.total_win_pip + self.total_lose_pip
        ammount_get_pips = ammount_get_pips

        # 最大連敗数
        if self.result_record[-1][6] == 'lose':
            self.losses_counter += 1
            if self.consecutive_losses_record < self.losses_counter:
                self.consecutive_losses_record = self.losses_counter
        else:
            self.losses_counter = 0
        # 最大連勝数
        if self.result_record[-1][6] == 'win':
            self.winess_counter += 1
            if self.consecutive_winess_record < self.winess_counter:
                self.consecutive_winess_record = self.winess_counter
        else:
            self.winess_counter = 0

        # ドローダウン
        drawdown = 0
        if self.max_account_balance <= self.account_balance:
            self.max_account_balance = self.account_balance

        else:
            drawdown = 100 - round(self.account_balance /
                                   self.max_account_balance * 100)
        if self.max_drawdown < drawdown:
            self.max_drawdown = drawdown

        # 期待値
        try:
            expected_value = self.total_win_pip / self.counter_win * win_late / \
                100 + self.total_lose_pip * -1 / \
                self.counter_lose * (win_late / 100 * -1)
        except ZeroDivisionError:
            expected_value = 0

        # 上昇率
        rate_of_up = self.account_balance / self.start_account_balance

        # 結果に追加
        # 損益
        profit = round(self.result_record[-1][4] * self.lot * 100)
        self.account_balance = self.account_balance + profit
        rate_of_change = profit / self.account_balance * 100
        rate_of_change = rate_of_change
        self.result_record[-1].append(self.lot)  # ロット数
        self.result_record[-1].append(profit)  # 損益
        self.result_record[-1].append(round(rate_of_change, 2)),  # 損益率

        self.assets_recored.append([
            '口座残高:' + str(round(self.account_balance)),  # 口座残高
            '取引総数:' + str(self.counter_win + self.counter_lose),
            '勝率:' + str(round(win_late)),  # 勝率
            'リスクリワードレシオ:' + str(round(risk_reward_ratio, 2)),  # リスクリワードレシオ
            '連敗数:' + str(self.consecutive_losses_record),  # 連敗数
            '連勝数:' + str(self.consecutive_winess_record),  # 連勝数
            '合計ピップ数:' + str(round(ammount_get_pips, 1)),  # 合計ピップ数
            'ドローダウン:' + str(self.max_drawdown),  # ドローダウン
            '期待値:' + str(round(expected_value, 2)),  # 期待値
            '上昇率:' + str(round(rate_of_up, 2))
        ])
