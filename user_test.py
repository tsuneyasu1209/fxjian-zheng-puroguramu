import indicater
import get_data
import trade_csv
import time
import csv

api = get_data.Api()
# 0:通貨 1:時間軸 2:スプ 3:口座金額 4:検証年 5:始まり月 6:終わり月
setting_list = ['USD_JPY', 'M5', '0.8', '300000', '2018', '01-01', '01-31']
years = []
sum_all = []

# 入ってきた設定のリストを取得
setting_result = get_data.setting_data(
    setting_list[0], setting_list[1], setting_list[2], setting_list[3], setting_list[4], setting_list[5], setting_list[6])
ex_piar = setting_result[0]  # 通貨
ex_piar_cut = setting_result[1]  # 通貨の分類
span = setting_result[2]  # 時間軸
spled = setting_result[3]  # スプレット
account_balance = setting_result[4]  # 口座残高
years.append(setting_result[5])  # 年
month = setting_result[6]  # 始月：終月

for year in years:
    for start, end in month.items():
        with open('data/' + ex_piar + '/' + year + '/' + span + '/' + year + start + year + end) as f:
            reader = csv.reader(f)
            data = [row for row in reader]
            del data[0]
            sum_all += data
# 全検証インデント
    # 年単位の検証インデント

# 設定
state = False
max_loss = 0.01  # 最大損失
stop_rate = 2  # 安値からのストップ幅
limit_rate = 200  # リミット、200で二倍

if ex_piar_cut == '_JPY':
    spled /= 100
    stop_rate /= 100
else:
    spled /= 10000
    stop_rate /= 10000
trade_pr = trade_csv.Trade(sum_all, account_balance,
                           max_loss, stop_rate, limit_rate)
indicaters = []
# インジケーター
indicaters_list = [{'simple_moving_average_5': 5},
                   {'simple_moving_average_20': 20}]

for k in range(len(indicaters_list)):
    for key, value in indicaters_list[k].items():
        key = indicater.Indicater()
        indicaters.append(key)


print('test start' + str(len(sum_all)))
start_time = time.time()

# date', 'open', 'high', 'low', 'close


for i in range(len(sum_all)):
    trade_pr.progress(i)  # 進行度の計測
    corrent_date = sum_all[i][0]
    corrent_open_price = float(sum_all[i][1])
    corrent_high_price = float(sum_all[i][2])
    corrent_low_price = float(sum_all[i][3])
    corrent_price = float(sum_all[i][4])
    # インジケーター
    entry_counter = 0
    result_list = []
# インジケーターの判定
    for p in range(len(indicaters)):
        indicaters[p].data_open = corrent_open_price
        indicaters[p].data_high = corrent_high_price
        indicaters[p].data_low = corrent_low_price
        indicaters[p].data_close = corrent_price
        indicaters[p].data_pre = float(sum_all[i - 1][4])
        for key, value in indicaters_list[p].items():
            ind_name = indicaters[p].sorting(key, value)
            result_list.append(ind_name)

    entry_counter += indicaters[0].compare_more_than(
        corrent_price, result_list[0])
    entry_counter += indicaters[0].compare_more_than(
        corrent_price, result_list[1])
    entry_counter += indicaters[0].compare_more_than(
        result_list[0], result_list[1])

    if entry_counter < 3 or entry_counter > -3:
        state = True
    # トレード条件
    # 買い
    # 売り

    # 売り

    # エントリー条件 売り
    if entry_counter == -3:
        if not trade_pr.trade_record or trade_pr.trade_record[-1][6] == 'off':
            state = False
            if ex_piar_cut == '_JPY':
                trade_pr.trade_record += trade_pr.entry_sell(i)
            else:
                trade_pr.trade_record += trade_pr.entry_sell_usd(i)
        else:
            pass
        # 負け条件
            if trade_pr.trade_record[-1][6] == 'sell' and trade_pr.trade_record[-1][2] < corrent_high_price + spled:
                if ex_piar_cut == '_JPY':
                    trade_pr.lose(i)
                else:
                    trade_pr.lose_usd(i)
                trade_pr.calculate()
        # 勝利条件
            elif trade_pr.trade_record[-1][6] == 'sell' and trade_pr.trade_record[-1][3] > corrent_low_price + spled:
                if ex_piar_cut == '_JPY':
                    trade_pr.win(i)
                else:
                    trade_pr.win_usd(i)
                trade_pr.calculate()

        # 買い エントリー条件
    elif entry_counter == 3:
        if not trade_pr.trade_record or trade_pr.trade_record[-1][6] == 'off':
            if ex_piar_cut == '_JPY':
                trade_pr.trade_record += trade_pr.entry_buy(i)
            else:
                trade_pr.trade_record += trade_pr.entry_buy_usd(i)
        else:
            pass
        # 負け条件
            if trade_pr.trade_record[-1][6] == 'buy' and trade_pr.trade_record[-1][2] > corrent_low_price - spled:
                if ex_piar_cut == '_JPY':
                    trade_pr.lose(i)
                else:
                    trade_pr.lose_usd(i)
                trade_pr.calculate()
        # 勝利条件
            elif trade_pr.trade_record[-1][6] == 'buy' and trade_pr.trade_record[-1][3] < corrent_high_price - spled:
                if ex_piar_cut == '_JPY':
                    trade_pr.win(i)
                else:
                    trade_pr.win_usd(i)
                trade_pr.calculate()

end_time = time.time()
take_time = round((end_time - start_time) / 60, 2)
print('テストタイム' + str(take_time))
print(trade_pr.counter_lose)
print(trade_pr.counter_win)
print(trade_pr.counter_win + trade_pr.counter_lose)
print(trade_pr.trade_record[-1])
print(trade_pr.result_record[-1])
print(trade_pr.assets_recored[-1])
