import os
import glob
import csv
import pandas as pd

# フォルダ中のパスを取得
DATA_PATH = "./usdjpy/"
All_Files = glob.glob('{}*.csv'.format(DATA_PATH))
dtype1 = {'date': str, 'time': 'str', 'open': float, 'high': float,
          'low': float, 'close': float, 'volume': float}
names1 = ['date', 'time', 'open', 'high', 'low', 'close', 'volume']
df = pd.read_csv(filepath_or_buffer='USDJPY.csv', dtype=dtype1,
                 header=0, index_col='date', names=names1, parse_dates=['date'])

data1 = {'open': df['open'].values, 'high': df['high'].values,
         'low': df['low'].values, 'close': df['close'].values, 'volume': df['volume'].values}
columns1 = ['open', 'high', 'low', 'close', 'volume']
index1 = df.index + pd.DateOffset(hours=3)
df2 = pd.DataFrame(data=data1, columns=columns1, index=index1)
df2.head()

df3 = df2.resample(rule='D').ohlc()
df4 = df2.resample(rule='D').sum()
data2 = {'open': df3['open']['open'].values, 'high': df3['high']['high'].values, 'low': df3['low']
         ['low'].values, 'close': df3['close']['close'].values, 'volume': df4['volume'].values}
columns2 = ['open', 'high', 'low', 'close', 'volume']
df5 = pd.DataFrame(data=data2, columns=columns2, index=df3.index).dropna()
df5.head()
# list = []
# for file in All_Files:
#     list.append(pd.read_csv(file))
# df = pd.concat(list, sort=False)

# df.to_csv('usdjpy.csv', encoding='utf_8')
