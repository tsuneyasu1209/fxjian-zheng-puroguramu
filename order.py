import indicater
import get_data
import time
import web
import datetime
import user


# 設定
api = get_data.Api()
user_id = user.user_id  # demo
password = user.password

span = "M5"
current = -1  # 最新の価格
close = -2  # 終値
close_four_price = api.get_span_price(api.oanda, span, close)  # 四本値
last_time = close_four_price['時間']  # 現時刻と比べるための変数
stop_rate = 0.02  # 安値からのストップ幅
limit_rate = 200  # リミット、200で二倍
max_loss = 0.01  # 最大損失％
sleep_time = 300  # 何分毎に動かすか 60 = 1m
# インジケーター
period = 5
average = indicater.Indicater()
sma5 = indicater.Indicater()
sma20 = indicater.Indicater()
for i in reversed(range(2, 40)):
    close_four_price = api.get_span_price(api.oanda, span, i * close)
    ma = average.set_yama(close_four_price['終値'])
    be = average.get_yama(ma, average.rate_3)
    ma_5 = sma5.simple_moving_average(5, close_four_price['終値'])
    ma_20 = sma20.simple_moving_average(20, close_four_price['終値'])
    try:
        if 'low' in be[-1]:
            low = be[-1]['low']
            print('low' + str(low))
        elif 'high' in be[-1]:
            high = be[-1]['high']
            print('high' + str(high))
        else:
            no = be[-1]
            print(no)

    except:
        pass

# ページ表示
web = web.Web(user_id, password)
web.userLogin()


# オーダー
while True:
    try:
        start_time = time.time()
        close_four_price = api.get_span_price(
            api.oanda, span, close)  # 時間、四本値取得
    except:
        print('error')

    if close_four_price['時間'] != last_time:
        last_time = close_four_price['時間']
        ma = average.set_yama(
            close_four_price['終値'])
        be = average.get_yama(ma, average.rate_3)
        sma_5 = sma5.simple_moving_average(5, close_four_price['終値'])
        sma_20 = sma20.simple_moving_average(20, close_four_price['終値'])
        position = web.position_info()  # 残高とポジションの有無
        web.get_usd_jpy_orderpage()  # オーダーページの呼び出し
        info = web.get_spled_and_balance()  # スプレッド、口座残高
        no = ''
        try:
            if 'low' in be[-1]:
                low = be[-1]['low']
            elif 'high' in be[-1]:
                high = be[-1]['high']
            else:
                no = be[-1]
            print('low:' + str(low))
            print('high:' + str(high))
            print('no:' + no)
            print(sma_5[0])
            print(sma_20[0])
            print(close_four_price['終値'])
        except:
            pass
        
        if info['spled'] <= 0.4 and no == 'no':
            # 買い
            if close_four_price['終値'] > ma_5[0] > ma_20[0] and close_four_price['終値'] > high and position:
                print('take5-buy')

                stop_and_limit = api.get_stop_limit_pips(  # ストップとリミットの計算
                    close_four_price, stop_rate, limit_rate)
                lot = api.lot_calculation(  # ロットの計算
                    info['balance'] - 2000000, max_loss, stop_and_limit['stop_buy_pip'])
                web.order(lot, stop_and_limit['stop_buy'],  # オーダー
                          stop_and_limit['limit_buy'], 'buy')
                end_time = time.time()
                take_time = end_time - start_time
                print(take_time)
                print(last_time)
                print(stop_and_limit)
                time.sleep(1)
                # 売り
            elif close_four_price['終値'] < ma_5[0] < ma_20[0] and close_four_price['終値'] < low and position:
                print('take5-sell')
                stop_and_limit = api.get_stop_limit_pips(  # ストップとリミットの計算
                    close_four_price, stop_rate, limit_rate)
                print('ok')
                lot = api.lot_calculation(  # ロットの計算
                    info['balance'] - 2000000, max_loss, stop_and_limit['stop_sell_pip'])
                print('ok')
                web.order(lot, stop_and_limit['stop_sell'],  # オーダー
                          stop_and_limit['limit_sell'], 'sell')
                print('ok')

                end_time = time.time()
                take_time = end_time - start_time
                print(take_time)
                print(last_time)
                print(stop_and_limit)
                time.sleep(1)
            else:
                web.close_usd_jpy_orderpage()
                print('take5-non')
                time.sleep(1)
        else:
            web.close_usd_jpy_orderpage()
            time.sleep(1)
    else:
        time.sleep(1)
