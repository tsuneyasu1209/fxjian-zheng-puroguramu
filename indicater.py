import numpy as np
import random
import get_data
import collections
import datetime

# api = get_data.Api()
# data1 = api.price("2019-03-01T19:11:59", "2019-01-01T19:12:00")
# data = api.save_csv(data1)


class Indicater(object):
    def __init__(self):
        self.rate = []
        self.rate_2 = []
        self.rate_3 = []
        self.rate_4 = []
        self.rate_5 = []
        self.rate_6 = []
        self.rate_7 = []
        self.data_open = 0
        self.data_high = 0
        self.data_low = 0
        self.data_close = 0
        self.data_pre = 0

    def base(self, period, data, rate):
        if len(rate) >= period:
            rate.append(float(data))
            del rate[0]
            return rate
        else:
            rate.append(float(data))
            return rate

    # 高値、安値、終値を任意期の数で集める、デフォは14

    def collect_price(self, takane, yasune, close, period=14, rate=None, rate_2=None, rate_3=None):
        if rate is None:
            rate = self.rate
        if rate_2 is None:
            rate_2 = self.rate_2
        if rate_3 is None:
            rate_3 = self.rate_3
        high_list = self.base(period, takane, rate)
        low_list = self.base(period, yasune, rate_2)
        close_list = self.base(period, close, rate_3)
        return {'high': high_list, 'low': low_list, 'close': close_list}

    def fibonacci(self, data_high, data_low):
        balance_price = data_high - data_low
        fibo_236 = balance_price * 0.236
        fibo_382 = balance_price * 0.382
        fibo_618 = balance_price * 0.618
        fibo_786 = balance_price * 0.786
        fibo_886 = balance_price * 0.886
        return {'high': {
            'fibo_236': data_high - fibo_236,
            'fibo_382': data_high - fibo_382,
            'fibo_618': data_high - fibo_618,
            'fibo_786': data_high - fibo_786,
            'fibo_886': data_high - fibo_886
        }, 'low': {
            'fibo_236': data_low + fibo_236,
            'fibo_382': data_low + fibo_382,
            'fibo_618': data_low + fibo_618,
            'fibo_786': data_low + fibo_786,
            'fibo_886': data_low + fibo_886
        }}
    # 陽線陰線の判断

    def judge_candle_state(self, corrent_price_open, corrent_price, yousen=None, insen=None, closs=None):
        if corrent_price_open - corrent_price < 0:
            candle_state = 'yousen'
        elif corrent_price_open - corrent_price > 0:
            candle_state = 'insen'
        else:
            candle_state = 'closs'
        return candle_state
# エントリー条件の大小 1:買い -1:売り 0:なし

    def compare_more_than(self, bigger, smaller):
        if bigger > smaller:
            return 1
        elif bigger < smaller:
            return -1
        else:
            return 0

    def compare_or(self, bigger, smaller):
        if bigger >= smaller:
            return 1
        elif bigger <= smaller:
            return -1
        else:
            return 0

# トレンド系 MA
    def simple_moving_average(self, period, data, rate=None):
        if rate is None:
            rate = self.rate
        ma_data = self.base(period, data, rate)
        average = np.mean(ma_data)
        return round(average, 6), ma_data

    def ema(self, period, data_pre, data_now, rate=None):
        if rate is None:
            rate = self.rate
        ema_data = self.base(period, data_pre, rate)
        ema_pre = sum(ema_data) / period
        a = 2 / (period + 1)
        ema = ema_pre + a * (data_now - ema_pre)

        return round(ema, 6)

# ボリバン

    def bollinger_band(self, period, data, alpha=2, rate=None):
        if rate is None:
            rate = self.rate
        bollinger_data = self.base(period, data, rate)
        mean = np.mean(bollinger_data)
        std = np.std(bollinger_data)
        upper = mean + (std * alpha)
        lower = mean - (std * alpha)
        return {'upper': round(upper, 3), 'lower': round(lower, 3)}
# 高値安値判断(終値ベースになっている)

    def set_yama(self, data, ma_period=5):

        ma = self.simple_moving_average(ma_period, data, self.rate)
        base = self.base(3, ma[0], self.rate_2)
        try:
            if base[0] > base[1] < base[2]:
                yama = 'down'

            elif base[0] < base[1] > base[2]:
                yama = 'up'
            else:
                yama = 'no'
            return yama
        except IndexError:
            pass

    def get_high_yama(self, data_high, ma_period=5):

        high_base = self.base(ma_period, data_high, self.rate_3)
        high_price = max(high_base)
        return high_price

    def get_low_yama(self, data_low, ma_period=5):

        low_base = self.base(ma_period, data_low, self.rate_4)
        low_price = min(low_base)
        return low_price

    def get_high_and_low_yama(self, corrent_price, corrent_high_price, corrent_low_price, yama_priod):
        yama = self.set_yama(corrent_price, yama_priod)
        high = self.get_high_yama(corrent_high_price, yama_priod)
        low = self.get_low_yama(corrent_low_price, yama_priod)
        return yama, high, low
# 一目均衡表

    def ichimoku(self, data, rate=None, rate_1=None, rate_2=None):
        if rate is None:
            rate = self.rate
        if rate_1 is None:
            rate_1 = self.rate
        if rate_2 is None:
            rate_2 = self.rate_2
        conversion_line_list = self.base(9, data, rate)
        standard_line_list = self.base(26, data, rate_1)

        conversion_line = (max(conversion_line_list) +
                           min(conversion_line_list)) / 2
        standard_line = (max(standard_line_list) + min(standard_line_list)) / 2

        delay_span = data

        preceding1 = (conversion_line + standard_line) / 2

        preceding2_list = self.base(52, data, rate_2)
        preceding2 = (max(preceding2_list) + min(preceding2_list)) / 2
        if preceding1 < data and preceding2 < data:
            cloud = 'support'
        elif preceding1 > data and preceding2 > data:
            cloud = 'resistance'
        else:
            cloud = 'none'
        return {'conversion_line': round(conversion_line, 3),
                'standard_line': round(standard_line, 3),
                'delay_span': round(delay_span, 3),
                'preceding1': round(preceding1, 3),
                'preceding2': round(preceding2, 3),
                'cloud': cloud
                }
# envelope

    def envelope(self, period, N, data, rate=None):
        # N=% 0.001
        if rate is None:
            rate = self.rate
        ma = self.simple_moving_average(period, data, rate)
        up_band = ma[0] + ma[0] * N
        low_band = ma[0] - ma[0] * N
        return {'sma': round(ma[0], 3), 'up_band': round(up_band, 3), 'low_band': round(low_band, 3)}
# ADX 改良必要

    def average_directional_movement_index(
        self,
        takane,
        yasune,
        close,
        period=14,
        adx_priod=14,
        rate=None,
        rate_2=None,
        rate_3=None,
        rate_4=None,
        rate_5=None,
        rate_6=None,
        rate_7=None
    ):
        # yasuneに安値、takaneに高値、closeに終値
        if rate_4 is None:
            rate_4 = self.rate_4
        if rate_5 is None:
            rate_5 = self.rate_5
        if rate_6 is None:
            rate_6 = self.rate_6
        if rate_7 is None:
            rate_7 = self.rate_7

        price_list = self.collect_price(takane, yasune, close, period)

        # DMの計算
        try:
            plus_dm = price_list['high'][-1] - price_list['high'][-2]
            minus_dm = price_list['low'][-2] - price_list['low'][-1]
            if plus_dm < 0:
                plus_dm = 0
            if minus_dm < 0:
                minus_dm = 0

            if plus_dm > minus_dm:
                minus_dm = 0
            if plus_dm < minus_dm:
                plus_dm = 0

            plus_dm_list = self.base(period, plus_dm, rate_4)
            minus_dm_list = self.base(period, minus_dm, rate_5)
            # TRの計算

            tr_a = price_list['high'][-1] - price_list['low'][-1]
            tr_b = price_list['high'][-1] - price_list['close'][-2]
            tr_c = price_list['close'][-2] - price_list['low'][-1]
            tr_list = [tr_a, tr_b, tr_c]
            tr = max(tr_list)

            result_tr_list = self.base(period, tr, rate_6)
            plus_DI = sum(plus_dm_list) / sum(result_tr_list) * 100
            minus_DI = sum(minus_dm_list) / sum(result_tr_list) * 100

            # adx計算
            DI = abs((plus_DI - minus_DI)) / (plus_DI + minus_DI)
            adx = self.simple_moving_average(adx_priod, DI, rate_7)
            return {'+DI': plus_DI, '-DI': minus_DI, 'ADX': adx[0]}

        except IndexError:
            pass
# オシレータ系
# ストキャス

    def stochastic_oscillator(self, data, rate=None, rate_2=None, period=9, period_ma=3):
        if rate is None:
            rate = self.rate
        if rate_2 is None:
            rate_2 = self.rate_2
        k_base = self.base(period, data, rate)
        if len(k_base) == period:
            percent_k = (data - min(k_base)) / \
                (max(k_base) - min(k_base)) * 100
            percent_d = self.simple_moving_average(
                period_ma, percent_k, rate_2)
            return {'k': round(percent_k, 2), 'd': round(percent_d[0], 2)}
# RSI

    def rsi(self, data, rate=None, rate_1=None, rate_2=None, rate_3=None, period=14):
        if rate is None:
            rate = self.rate
        if rate_1 is None:
            rate_1 = self.rate_2
        if rate_2 is None:
            rate_2 = self.rate_3
        if rate_3 is None:
            rate_3 = self.rate_4
        rsi_list = self.base(period, data, rate)

        try:
            result = rsi_list[-2] - rsi_list[-1]
            result_list = self.base(period, result, rate_1)
            rate_2 = []
            rate_3 = []
            for i in range(len(result_list)):
                if result_list[i] > 0:
                    rate_2.append(result_list[i])
                else:
                    rate_3.append(result_list[i])
            a = sum(rate_2) / period
            b = sum(rate_3) * -1 / period
            result_rsi = a / (a + b) * 100
            return round(result_rsi, 2)
        except IndexError:
            pass
# ATR

    def average_true_range(self, takane, yasune, close, period=20, rate=None,):
        if rate is None:
            rate = self.rate_4

        price_list = self.collect_price(takane, yasune, close, period)
        try:
            tr_a = price_list['high'][-1] - price_list['low'][-1]
            tr_b = price_list['high'][-1] - price_list['close'][-2]
            tr_c = price_list['close'][-2] - price_list['low'][-1]
            tr_list = [tr_a, tr_b, tr_c]
            tr = max(tr_list)
            # print(tr)
            result = self.simple_moving_average(period, tr, rate)
            return result[0]
        except IndexError:
            pass
# Williams ％R

    def williams_percent_range(self, takane, yasune, close, period=20, rate=None, rate_2=None, rate_3=None):
        price_list = self.collect_price(takane, yasune, close, period)
        max_price = (max(price_list['high']))
        min_price = (min(price_list['low']))
        now_price = price_list['close'][-1]
        result = (max_price - now_price) / (max_price - min_price) * 100
        return round(result, 1)

        # MACD
        # 改良が必要

    def macd(self, data_pre, data_now, short_period=12, long_period=26, signal=9, rate=None, rate_2=None, rate_3=None):
        if rate is None:
            rate = self.rate
        if rate_2 is None:
            rate_2 = self.rate_2
        if rate_3 is None:
            rate_3 = self.rate_3
        short_ema = self.ema(short_period, data_pre, data_now, rate)
        long_ema = self.ema(long_period, data_pre, data_now, rate_2)
        try:
            macd = short_ema - long_ema
            signal_ema = self.simple_moving_average(signal, macd, rate_3)
            histogram = macd - signal_ema[0]
            return {'macd': round(macd, 7), 'signal': round(signal_ema[0], 7), 'histogram': round(histogram, 7)}
        except IndexError:
            pass
# Bears and Bulls Power

    def bears_or_bulls_power(self, data_pre, data_now, data_now_high, data_now_low, short_period=13, rate=None):
        if rate is None:
            rate = self.rate

        ema = self.ema(short_period, data_pre, data_now, rate)
        bulls_power_resulf = data_now_high - ema
        bears_power_resulf = data_now_low - ema
        return {'bears': bears_power_resulf, 'bulls': bulls_power_resulf}
# CCI改良が必要

    def commodity_channel_index(self, data_high, data_low, data_close, period=14, rate=None, rate_2=None, rate_3=None):
        if rate is None:
            rate = self.rate
        if rate_2 is None:
            rate_2 = self.rate_2
        if rate_3 is None:
            rate_3 = self.rate_3
        center_price = (data_high + data_low + data_close) / 3
        center_average = self.simple_moving_average(period, center_price, rate)
        result = abs(center_price - center_average[0])
        std = self.simple_moving_average(period, result, rate_3)
        cci = (center_price - center_average[0]) / (0.015 * std[0])
        return cci
# Fore Index

    def force_index(self, data, data_pre, volum, period=13, rate=None, rate_2=None):
        if rate is None:
            rate = self.rate
        if rate_2 is None:
            rate_2 = self.rate_2
        sma = self.simple_moving_average(period, data, rate)
        sma_pre = self.simple_moving_average(period, data_pre, rate_2)
        force_index = volum * (sma[0] - sma_pre[0])
        return force_index

# Momentum

    def momentum(self, data, period=14, rate=None):
        if rate is None:
            rate = self.rate
        momentum_list = self.base(period, data, rate)
        result = momentum_list[-1] - momentum_list[0]
        return result

# OsMA
    def moving_average_of_oscillator(self, data_pre, data_now, short_period=12, long_period=26, signal=9, rate=None, rate_2=None, rate_3=None):
        if rate is None:
            rate = self.rate
        if rate_2 is None:
            rate_2 = self.rate_2
        if rate_3 is None:
            rate_3 = self.rate_3
        macd_result = self.macd(data_pre, data_now)
        osma = macd_result['macd'] - macd_result['signal']
        return round(osma, 4)

# インジケーターの選別機
    def sorting(self, indicater_name, period, data_open=None, data_high=None, data_low=None, data_close=None, data_pre=None):
        if data_open is None:
            data_open = self.data_open
        if data_high is None:
            data_high = self.data_high
        if data_low is None:
            data_low = self.data_low
        if data_close is None:
            data_close = self.data_close
        if data_pre is None:
            data_pre = self.data_pre
        # トレンド系
        # sma
        if 'simple_moving_average' in indicater_name:
            result = self.simple_moving_average(period, data_close)
            return result[0]
        # ema
        if 'ema' in indicater_name:
            result = self.ema(period, data_pre, data_close)
            return result
        # bollinger_band
        if 'bollinger_band' in indicater_name:
            result = self.bollinger_band(period, data_close)
            return result
        # ichimoku
        if 'ichimoku' in indicater_name:
            result = self.ichimoku(data_close)
            return result
        # # envelope
        # if 'envelope' in indicater_name:
        #     result = self.envelope(period,)
        # ADX
        if 'average_directional_movement_index' in indicater_name:
            result = self.average_directional_movement_index(
                data_high, data_low, data_close)
            return result
        # オシレーター系
        # stochastic_oscillator
        if 'stochastic_oscillator' in indicater_name:
            result = self.stochastic_oscillator(data_close)
            return result
        # rsi
        if 'rsi' in indicater_name:
            result = self.rsi(data_close)
            return result
        # average_true_range
        if 'average_true_range' in indicater_name:
            result = self.average_true_range(
                data_high, data_low, data_close)
            return result
        # williams_percent_range
        if 'williams_percent_range' in indicater_name:
            result = self.williams_percent_range(
                data_high, data_low, data_close)
            return result
        # macd
        if 'macd' in indicater_name:
            result = self.macd(data_pre, data_close)
            return result
        # # CCI
        # if 'commodity_channel_index' in indicater_name:
        #     resutl = self.commodity_channel_index(
        #         data_high, data_low, data_close)
        #     return result
        # momentum
        if 'momentum' in indicater_name:
            result = self.momentum(data_close, period)
            return result
        # OsMA
        if 'moving_average_of_oscillator' in indicater_name:
            result = self.moving_average_of_oscillator(data_pre, data_close)
            return result


class Daw(object):
    def __init__(self):
        self.high_price_list = []
        self.low_price_list = []
        self.latest_high_price_list = []
        self.latest_low_price_list = []
        self.trend_counter_list = []
        self.trend_counter_list_result = []
        self.moved_price_list = []
        self.latest_high_price = 0
        self.latest_low_price = 0
        self.judge_number = 0
        self.trend_counter = 0
        self.counter_stopper = True
        self.trend_state = ''
        self.blake_counter_small = 0
        self.blake_counter_2 = 0
        self.blake_counter_3 = 0
        self.blake_counter_4 = 0
        self.blake_counter_5 = 0
        self.blake_counter_over = 0

        self.judge_price_high = 0
        self.judge_price_low = 0
        self.judge_price_state_list = []
        self.candle_flow_list = []
        self.candle_now_state_list = []
        self.candle_trend_counter = 0

        self.trend_line_set_high_price_list = []
        self.trend_line_set_low_price_list = []
        self.trend_line_start_price = 0
        self.yama_counter = 0
        self.state_of_yama = 0
        self.patarn_setup = False
        self.patarn_on = False
        self.patarn_state = ''
        self.patarn_state_list = []
        # トレンドラインの変数
        self.high_trendline_span_list = []
        self.low_trendline_span_list = []
        self.start_to_middle_high = 0
        self.start_to_middle_low = 0
        self.start_to_end_high = 0
        self.start_to_end_low = 0
        self.trendline_low_set = False
        self.trendline_high_set = False
        self.trendline_price_high_set = False
        self.trendline_price_low_set = False

    def get_high_and_low_list(self, yama, high, low):
        if 'up' == yama:
            self.high_price_list.append(high)

        elif 'down' == yama:
            self.low_price_list.append(low)

    def get_trendline_span(self, yama, high, low, i, data):

        try:
            for num in range(i)[::-1]:
                if float(data[num][2]) == self.high_price_list[-1]:
                    self.high_trendline_span_list.append(num)
                    print('high')
                    print(data[num][0])
                    print(self.high_trendline_span_list[-1])
                    break
        except IndexError:
            pass

    # レンジブレイク上

    def get_daw_state(self, corrent_price):
        try:
            if self.trend_state == 'range' and self.trend_counter >= 1 and self.latest_high_price < corrent_price:
                self.latest_high_price = self.high_price_list[-1]
                self.latest_low_price = self.low_price_list[-1]
                self.trend_counter += 1
                self.trend_state = 'up-range-blake'
                self.counter_stopper = True
                self.judge_number += 1

            # トレンド上
            elif self.trend_counter == 0 or self.trend_counter >= 1 and self.latest_high_price < corrent_price:
                if self.latest_high_price < self.high_price_list[-1]:
                    self.counter_stopper = True
                    self.latest_high_price = self.high_price_list[-1]
                if 'change' not in self.trend_state and self.judge_number == 0:
                    if self.counter_stopper and self.latest_high_price < corrent_price:
                        self.trend_counter += 1
                        self.counter_stopper = False
                        self.latest_low_price = self.low_price_list[-1]

                self.trend_state = 'up-trend'
                self.judge_number += 1

            # レンジブレイク下
            if self.trend_state == 'range' and self.trend_counter <= -1 and self.latest_low_price > corrent_price:
                self.latest_high_price = self.high_price_list[-1]
                self.latest_low_price = self.low_price_list[-1]
                self.judge_number += 1
                self.trend_counter -= 1
                self.trend_state = 'down-range-blake'
                self.counter_stopper = True

            # トレンド下
            elif self.trend_counter == 0 or self.trend_counter <= -1 and self.high_price_list[-2] > self.high_price_list[-1] and self.latest_low_price > corrent_price:
                if self.latest_low_price > self.low_price_list[-1]:
                    self.counter_stopper = True
                self.latest_low_price = self.low_price_list[-1]
                if 'change' not in self.trend_state and self.judge_number == 0:
                    if self.counter_stopper and self.latest_low_price > corrent_price:
                        self.trend_counter -= 1
                        self.counter_stopper = False
                        self.latest_high_price = self.high_price_list[-1]
                        self.judge_number += 1
                self.trend_state = 'down-trend'
                self.judge_number += 1

            # 上昇トレンド,レンジ,安値割り転換
            if self.trend_counter >= 1 and self.trend_state == 'range' and self.latest_low_price > corrent_price:
                self.latest_high_price = self.high_price_list[-1]
                self.trend_counter = -1
                self.trend_state = 'down-range-change_blake'
                self.counter_stopper = True
                self.judge_number = 1

            # 下降トレンド,レンジ,高値割り転換
            elif self.trend_counter <= -1 and self.trend_state == 'range' and self.latest_high_price < corrent_price:
                self.latest_low_price = self.low_price_list[-1]
                self.trend_counter = 1
                self.trend_state = 'up-range-change-blake'
                self.counter_stopper = True
                self.judge_number = 1
            # 上昇トレンド安値割り転換
            if self.trend_counter <= -1 and self.trend_state == 'down-change-blake' and self.latest_low_price > self.low_price_list[-1]:
                self.latest_low_price = self.low_price_list[-1]
                self.trend_state = 'down-changed'
            elif self.trend_counter >= 1 and self.latest_low_price > corrent_price:
                self.latest_high_price = self.high_price_list[-1]
                self.trend_counter = -1
                self.trend_state = 'down-change-blake'
                self.counter_stopper = True
                self.judge_number = 1

            # 下降トレンド高値割り転換
            if self.trend_counter >= 1 and self.trend_state == 'up-change-blake' and self.latest_high_price < self.high_price_list[-1]:
                self.latest_high_price = self.high_price_list[-1]
                self.trend_state = 'up-changed'
            elif self.trend_counter <= -1 and self.latest_high_price < corrent_price:
                self.latest_low_price = self.low_price_list[-1]
                self.trend_counter = 1
                self.trend_state = 'up-change-blake'
                self.counter_stopper = True
                self.judge_number += 1

            # レンジ
            if self.latest_low_price < self.low_price_list[-1] and self.latest_high_price > self.high_price_list[-1] and self.latest_low_price < corrent_price < self.latest_high_price:
                if self.trend_state == 'range':
                    self.trend_state = 'range'
                    self.judge_number = 0
                    self.counter_stopper = True
                else:
                    self.trend_state = 'range'
                    self.counter_stopper = True
                    self.judge_number = 0
            # 高値安値の代入
            if self.trend_counter >= 1 and self.latest_high_price < self.high_price_list[-1]:
                self.latest_high_price = self.high_price_list[-1]

            elif self.trend_counter <= -1 and self.latest_low_price > self.low_price_list[-1]:
                self.latest_low_price = self.low_price_list[-1]
            if self.latest_low_price < corrent_price < self.latest_high_price:
                self.judge_number = 0

            if self.trend_counter >= 1 and self.latest_high_price < self.high_price_list[-1]:
                self.latest_high_price = self.high_price_list[-1]

            elif self.trend_counter <= -1 and self.latest_low_price > self.low_price_list[-1]:
                self.latest_low_price = self.low_price_list[-1]
            if self.latest_low_price < corrent_price < self.latest_high_price:
                self.judge_number = 0
        except IndexError:
            pass
    # トレンドの長さの測定

    def get_price_width(self):
        self.latest_high_price_list.append(self.latest_high_price)
        self.latest_low_price_list.append(self.latest_low_price)
        self.trend_counter_list.append(self.trend_counter)

    def judge_daw_state(self, yama_list, corrent_price):
        self.get_high_and_low_list(yama_list[0], yama_list[1], yama_list[2])
        self.get_daw_state(corrent_price)
        self.get_price_width()

    def show_price_data(self, moved_price_list, trend_counter_list_result):
        for move in moved_price_list:
            if move < 0.5:
                self.blake_counter_small += 1
            elif 0.7 > move >= 0.5:
                self.blake_counter_2 += 1
            elif 0.9 > move >= 0.7:
                self.blake_counter_3 += 1
            elif 1.1 > move >= 0.9:
                self.blake_counter_4 += 1
            elif 1.3 > move >= 1.1:
                self.blake_counter_5 += 1
            elif move >= 1.3:
                self.blake_counter_over += 1
        ll = collections.Counter(trend_counter_list_result)
        print(ll)
        print('-------------')
        print('総数:' + str(len(moved_price_list)))
        print('平均:' + str(sum(moved_price_list) / len(moved_price_list)))
        print('0-2:' + str(round(self.blake_counter_small /
                                 len(moved_price_list) * 100)) + '%')
        print('2-4:' + str(round(self.blake_counter_2 /
                                 len(moved_price_list) * 100)) + '%')
        print('4-6:' + str(round(self.blake_counter_3 /
                                 len(moved_price_list) * 100)) + '%')
        print('6-8:' + str(round(self.blake_counter_4 /
                                 len(moved_price_list) * 100)) + '%')
        print('8-1:' + str(round(self.blake_counter_5 /
                                 len(moved_price_list) * 100)) + '%')
        print('1-0:' + str(round(self.blake_counter_over /
                                 len(moved_price_list) * 100)) + '%')

    def get_candle_counter(self,
                           corrent_price,
                           corrent_high_price,
                           corrent_low_price,
                           judge_price_high=None,
                           judge_price_low=None,
                           judge_price_state_list=None,
                           candle_flow_list=None,
                           candle_now_state_list=None,
                           candle_trend_counter=None
                           ):

        if judge_price_high is None:
            judge_price_high = self.judge_price_high
        if judge_price_low is None:
            judge_price_low = self.judge_price_low
        if judge_price_state_list is None:
            judge_price_state_list = self.judge_price_state_list
        if candle_flow_list is None:
            candle_flow_list = self.candle_flow_list
        if candle_now_state_list is None:
            candle_now_state_list = self.candle_now_state_list
        if candle_trend_counter is None:
            candle_trend_counter = self.candle_trend_counter

        if judge_price_high >= corrent_price >= judge_price_low:
            judge_price_state = 'harami'

        elif judge_price_high < corrent_price:
            self.judge_price_high = corrent_high_price
            self.judge_price_low = corrent_low_price
            judge_price_state = 'high'

        elif judge_price_low > corrent_price:
            self.judge_price_high = corrent_high_price
            self.judge_price_low = corrent_low_price
            judge_price_state = 'low'
        self.judge_price_state_list.append(judge_price_state)

        try:
            if self.judge_price_state_list[-1] != self.judge_price_state_list[-2]:
                candle_flow = self.judge_price_state_list[-1]
                self.candle_flow_list.append(candle_flow)

                if self.candle_flow_list[-1] == 'high':
                    if self.candle_flow_list[-2] == 'harami' and self.candle_flow_list[-3] == 'high':
                        candle_now_state = 'high-harami-blake'

                    if self.candle_flow_list[-2] == 'low':
                        candle_now_state = 'change-high'
                        self.candle_trend_counter = 0
                    if self.candle_flow_list[-2] == 'harami' and self.candle_flow_list[-3] == 'low':
                        candle_now_state = 'harami-change-high'
                        self.candle_trend_counter = 0

                elif self.candle_flow_list[-1] == 'low':
                    if self.candle_flow_list[-2] == 'harami' and self.candle_flow_list[-3] == 'low':
                        candle_now_state = 'low-harami-blake'
                    if self.candle_flow_list[-2] == 'high':
                        candle_now_state = 'change-low'
                        self.candle_trend_counter = 0
                    if self.candle_flow_list[-2] == 'harami' and self.candle_flow_list[-3] == 'high':
                        candle_now_state = 'harami-change-low'
                        self.candle_trend_counter = 0

                elif self.candle_flow_list[-1] == 'harami':
                    if self.candle_flow_list[-2] == 'low':
                        candle_now_state = 'low-harami'
                    if self.candle_flow_list[-2] == 'high':
                        candle_now_state = 'high-harami'

                candle_now_state_list.append(candle_now_state)
            if self.candle_flow_list[-1] == 'high':
                self.candle_trend_counter += 1
            elif self.candle_flow_list[-1] == 'low' and 'change':
                self.candle_trend_counter -= 1

        except IndexError:
            pass
        except NameError:
            pass

    def judge_patern(self, corrent_price):
        try:
            if self.latest_high_price_list[-1] > corrent_price > self.latest_low_price_list[-1]:
                self.patarn_setup = True
                if self.low_price_list[-2] < self.low_price_list[-1] and self.high_price_list[-2] > self.high_price_list[-1] and not self.patarn_on:
                    self.patarn_state = 'w'
                    self.patarn_on = True
                else:
                    self.patarn_state = ''
            else:
                self.patarn_setup = False
                self.patarn_on = False
            self.patarn_state_list.append(self.patarn_state)
        except IndexError:
            pass
        # トレンドラインに使うプライスを集める関数

    def get_torendline_price(self, corrent_price, i, data):
        # トレンドラインに使うプライスの収集
        try:
            if self.trend_counter_list[-2] != self.trend_counter_list[-1] and 1 < self.trend_counter_list[-1]:
                low = [self.latest_low_price_list[-2],
                       self.latest_low_price_list[-1]]
                self.trend_line_set_low_price_list.append(low)
                for num in range(i)[::-1]:
                    if float(data[num][3]) == self.trend_line_set_low_price_list[-1][1]:
                        self.low_trendline_span_list.append(num)
                        break
                for nn in range(self.low_trendline_span_list[-1] + 5)[::-1]:
                    if float(data[nn][3]) == self.trend_line_set_low_price_list[-1][0]:
                        self.low_trendline_span_list.append(nn)
                        break
                self.start_to_middle_low = (self.low_trendline_span_list[-2] -
                                            self.low_trendline_span_list[-1])
                # print('---------------------')
                # print(data[self.low_trendline_span_list[-1]][0])
                # print(data[self.low_trendline_span_list[-2]][0])
                # print(self.trend_line_set_low_price_list[-1])
                # print(self.start_to_middle_low)

            if self.start_to_middle_low != 0 and self.trend_counter > 0:
                self.start_to_end_low = (self.low_trendline_span_list[-2] - i) * -1
                trend_line = round(self.trend_line(
                    self.trend_line_set_low_price_list[-1][0],
                    self.trend_line_set_low_price_list[-1][1],
                    self.start_to_end_low,
                    self.start_to_middle_low
                ), 3)
                # print('---------------------')
                # print(data[i][0])
                # print(self.trend_line_set_low_price_list[-1][0])
                # print(self.trend_line_set_low_price_list[-1][1])
                # print(self.start_to_middle_low)
                # print(self.start_to_end_low)
                # print(trend_line)

            if self.trend_counter_list[-2] != self.trend_counter_list[-1] and 1 > self.trend_counter_list[-1]:
                high = [self.latest_high_price_list[-2],
                        self.latest_high_price_list[-1]]
                self.trend_line_set_high_price_list.append(high)
                for num in range(i)[::-1]:
                    if float(data[num][2]) == self.trend_line_set_high_price_list[-1][1]:
                        self.high_trendline_span_list.append(num)
                        break
                for nn in range(self.high_trendline_span_list[-1] + 5)[::-1]:
                    if float(data[nn][2]) == self.trend_line_set_high_price_list[-1][0]:
                        self.high_trendline_span_list.append(nn)
                        break
                self.start_to_middle_high = (self.high_trendline_span_list[-2] -
                                             self.high_trendline_span_list[-1])
                # print('---------------------')
                # print(data[self.high_trendline_span_list[-1]][0])
                # print(data[self.high_trendline_span_list[-2]][0])
                # print(self.trend_line_set_high_price_list[-1])
                # print(self.start_to_middle_high)
            if self.start_to_middle_high != 0 and self.trend_counter < 0:
                self.start_to_end_high = (self.high_trendline_span_list[-2] - i) * -1
                trend_line = round(self.trend_line(
                    self.trend_line_set_high_price_list[-1][0],
                    self.trend_line_set_high_price_list[-1][1],
                    self.start_to_end_high,
                    self.start_to_middle_high
                ), 3)
                # print('---------------------')
                # print(data[i][0])
                # print(self.trend_line_set_high_price_list[-1][0])
                # print(self.trend_line_set_high_price_list[-1][1])
                # print(self.start_to_end_high)
                # print(self.start_to_middle_high)
                # print(trend_line)

        except IndexError:
            pass


# 波の長さ測定
# latest_high_price_list.append(daw.latest_high_price)
# latest_low_price_list.append(daw.latest_low_price)
# trend_counter_list.append(daw.trend_counter)

# if daw.trend_counter == 1 and daw.judge_number == 1:
#     moved_price_low = latest_low_price_list[-1]
#     if latest_low_price_list[-2] > latest_high_price_list[-1]:
#         moved_price = moved_price_high - \
#             latest_low_price_list[-1]
#         moved_price_list.append(round(moved_price, 3))
#         trend_counter_list_result.append(
#             trend_counter_list[-2])

#     elif latest_low_price_list[-2] < latest_high_price_list[-1]:
#         moved_price = moved_price_high - \
#             latest_low_price_list[-2]
#         moved_price_list.append(round(moved_price, 3))
#         trend_counter_list_result.append(
#             trend_counter_list[-2])

# if daw.trend_counter == -1 and daw.judge_number == 1:
#     moved_price_high = latest_high_price_list[-1]
#     if latest_high_price_list[-2] < latest_high_price_list[-1]:
#         moved_price = latest_high_price_list[-1] - \
#             moved_price_low
#         moved_price_list.append(round(moved_price, 3))
#         trend_counter_list_result.append(
#             trend_counter_list[-2])

#     elif latest_high_price_list[-2] > latest_high_price_list[-1]:
#         moved_price = latest_high_price_list[-2] - \
#             moved_price_low
#         moved_price_list.append(round(moved_price, 3))
#         trend_counter_list_result.append(
#             trend_counter_list[-2])


def summer_flag(ref_date):
    # 指定された年の3月の第2日曜日を探す
    count_sunday = 0
    ref_1 = datetime.datetime(year=int(ref_date), month=3, day=1)
    for i in range(20):
        ref_1 = ref_1 + datetime.timedelta(days=1)
        if ref_1.weekday() == 6:
            count_sunday += 1
            if count_sunday == 2:
                summer_start = ref_1
                break
    # 指定された年の11月の最終日曜日を探す
    ref_2 = datetime.datetime(year=int(ref_date), month=10, day=31)
    for i in range(10):
        ref_2 = ref_2 + datetime.timedelta(days=1)
        if ref_2.weekday() == 6:
            summer_end = ref_2
            break
    return summer_start, summer_end


def judge_summer_time(ref_date, summer, mt_time):
    # 夏時間期間中か判定
    try:
        if ref_date >= summer[0][0] and ref_date <= summer[0][1]:
            a = datetime.timedelta(hours=mt_time + 1)
        else:
            a = datetime.timedelta(hours=mt_time)
        if ref_date >= summer[0][1]:
            del summer[0]
    except IndexError:
        a = datetime.timedelta(hours=2)
    return a
