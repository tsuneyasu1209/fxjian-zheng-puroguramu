import indicater
import get_data
import time
import web
import user


# 設定
api = get_data.Api()
user_id = user.user_id # demo
password = user.password
web = Web(user_id, password)
span = "M5"
current = -1  # 最新の価格
close = -2  # 終値
close_four_price = api.get_span_price(api.oanda, span, close)  # 四本値
last_time = close_four_price['時間']  # 現時刻と比べるための変数
stop_rate = 0.01  # 安値からのストップ幅
limit_rate = 150  # リミット、200で二倍
max_loss = 0.01  # 最大損失
sleep_time = 300  # 何分毎に動かすか 60 = 1m
# インジケーター
period = 5
indicater = indicater.Indicater()
for i in reversed(range(2, 8)):
    close_four_price = api.get_span_price(api.oanda, span, i * -1)
    ma = indicater.simple_moving_average(
        period, close_four_price['終値'])
# api版
while True:
    start_time = time.time()
    close_four_price = api.get_span_price(api.oanda, span, close)  # 時間、四本値取得
    balance_and_position = api.get_balance()  # 残高とポジションの有無
    if close_four_price['時間'] != last_time:
        last_time = close_four_price['時間']

        ma = indicater.simple_moving_average(
            period, close_four_price['終値'])

        if balance_and_position['position'] == 0 and close_four_price['終値'] > ma:

            stop_and_limit = api.get_stop_limit_pips(
                close_four_price, stop_rate, limit_rate)
            lot = api.lot_calculation(
                balance_and_position['balance'], max_loss, stop_and_limit['stop_buy_pip'])
            api.oder(stop_and_limit['stop_buy'],
                     stop_and_limit['limit_buy'], lot)
            print(stop_and_limit)
            end_time = time.time
            take_time = end_time - start_time
            time.sleep(sleep_time - take_time)
        else:
            end_time = time.time
            take_time = end_time - start_time
            time.sleep(sleep_time - take_time)

    else:
        end_time = time.time()
        take_time = end_time - start_time
        time.sleep(sleep_time - take_time)
