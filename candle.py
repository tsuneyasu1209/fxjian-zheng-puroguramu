class movingAverage():

    def maBase(self, data, ave):
        ma = 0
        for i in range(ave):
            ma += float(data[i][4])
        ma /= ave
        return ma

    def maCandle(self, ma, i, data, ave):
        ma *= ave
        ma += float(data[i][4])
        ma -= float(data[i-ave][4])
        ma /= ave
        return ma

    def maTrend(self, ma, i, data):

        if ma < float(data[i][4]):
            trend = 'bull'

        elif ma > float(data[i][4]):
            trend = 'bear'
        else:
            trend = 'no'
        return trend

    def maDeviationRate(self, ma, i, data):
        if ma <= float(data[i][1]):
            kai = ma / float(data[i][1])
        else:
            kai = float(data[i][1]) / ma
        return kai

    def candleTrend(self, i, data, tpl, tph):
        target_price_low = tpl
        target_price_hight = tph

        current_price = float(data[i][4])
        if target_price_low <= current_price <= target_price_hight:
            get_ready = 'ready'
        elif target_price_low > current_price:
            get_ready = 'not'
            target_price_low = float(data[i][3])
            target_price_hight = float(data[i][2])
        elif target_price_hight < current_price:
            get_ready = 'not'
            target_price_low = float(data[i][3])
            target_price_hight = float(data[i][2])
        else:
            pass
        return get_ready, target_price_low, target_price_hight
    #  ['date','open', 'high', 'low', 'close']


movingAverage = movingAverage()
ma5 = movingAverage.maBase(sum_all, 5)
ma21 = movingAverage.maBase(sum_all, 21)
ma100 = movingAverage.maBase(sum_all, 100)
candle_trend = ['', float(0.1), float(0.1)]

trade = trade()
trade_record = []
win_record = []
lose_record = []
result_data = {}
counter_win = 0
counter_lose = 0
for i in range(21, 100):
    ma21 = movingAverage.maCandle(ma5, i, sum_all, 21)
    ma100 = movingAverage.maCandle(ma100, i, sum_all, 100)
    ma_trend21 = movingAverage.maTrend(ma21, i, sum_all)
    ma_trend100 = movingAverage.maTrend(ma100, i, sum_all)

    candle_trend = movingAverage.candleTrend(
        i, sum_all, candle_trend[1], candle_trend[2])
    if not trade_record or trade_record[-1][6] == 'off':
        if i >= 101:
            if candle_trend[0] == 'ready' and ma_trend21 == 'bull':
                if candle_trend[0] == 'ready' and ma_trend100 == 'bull':

                    if candle_trend[2] < float(sum_all[i+1][4]):
                        trade_record += trade.entry(i, sum_all)
                        if trade_record[-1][4] <= float(sum_all[i+1][4]):
                            trade.win(i, sum_all, trade_record)
                            counter_win += 1
                            print('win')
                            print(counter_win)
                        elif trade_record[-1][2] >= float(sum_all[i+1][4]):
                            trade.lose(i, sum_all, trade_record)
                            counter_lose += 1
                            print('lose')
                            print(counter_lose)

            elif candle_trend[0] == 'ready' and ma_trend21 == 'bull':
                if candle_trend[0] == 'ready' and ma_trend100 == 'bull':
                    if candle_trend[2] < float(sum_all[i+1][4]):
                        trade_record += trade.entry(i, sum_all)
                        if trade_record[-1][5] >= float(sum_all[i+1][4]):
                            trade.win(i, sum_all, trade_record)
                            counter_win += 1
                            print('win')
                            print(counter_win)
                        elif trade_record[-1][3] <= float(sum_all[i+1][4]):
                            trade.lose(i, sum_all, trade_record)
                            counter_lose += 1
                            print('lose')
                            print(counter_lose)
print(counter_win/(counter_win+counter_lose))
