import datetime
import json
import math
import os
import time
from datetime import datetime, timedelta

import oandapyV20
import oandapyV20.endpoints.accounts as accounts
import oandapyV20.endpoints.instruments as instruments
import oandapyV20.endpoints.orders as orders
import oandapyV20.endpoints.positions as positions
import oandapyV20.endpoints.pricing as pricing
import pandas as pd
import pytz
from oandapyV20 import API
from oandapyV20.endpoints.accounts import AccountSummary
from oandapyV20.endpoints.instruments import InstrumentsCandles
from oandapyV20.endpoints.pricing import PricingStream
from oandapyV20.exceptions import V20Error


class Api():
    def __init__(self):
        self.accountID = "101-009-13075687-001"
        self.access_token = "261fe81beadcb3c0133a06affcf9bb1d-06694e000eecd0e165df7b95228336f9"
        self.oanda = oandapyV20.API(
            access_token=self.access_token, environment="practice")

    # "2019-01-01T19:12:00"
    # どの期間まで取得したいか指定(年-月-日T時:分の形式で指定、時間はなくても良い(おそらく))

    def price(self, end, get_date):
        ex_pair = "USD_JPY"
        asi = 'M5'
        get_date_csv = get_date[:10]
        end_csv = end[:10]
        i = 0
        while(end > get_date):
            params = {"count": 5000, "granularity": asi, "to": end}
            r = instruments.InstrumentsCandles(
                instrument=ex_pair, params=params,)
            apires = self.oanda.request(r)
            res = r.response['candles']
            end = res[0]['time']
            n = 0
            if i == 0:
                res1 = res
            else:
                for j in range(n, len(res1)):
                    res.append(res1[j])
                if end < get_date:
                    for j in range(5000):
                        if res[j]['time'] > get_date:
                            end = res[j - 1]['time']
                            n = j
                            break
            res1 = res[n:]
            i += 1
        data = []
        # 少し形を成形してあげる
        for raw in res1:
            data.append([raw['time'], raw['mid']['o'], raw['mid']
                         ['h'], raw['mid']['l'], raw['mid']['c']])
        df = pd.DataFrame(data)
        df.columns = ['date', 'open', 'high', 'low', 'close']

        # 時間を全て日本時間に変更する。
        for i in df['date']:
            i = pd.Timestamp(i).tz_convert('Asia/Tokyo')
        # csv保存
        df = df.set_index('date')
        df.index = df.index.astype('datetime64')
        df.to_csv('data/' + get_date_csv + '_' +
                  end_csv + '.csv', encoding='UTF8')
        return data

    # 文字列 -> datetime

    def save(self, data):
        # DataFrameに変換して、CSVファイルに保存をする。
        df = pd.DataFrame(data)
        df.columns = ['date', 'open', 'high', 'low', 'close']

        # 時間を全て日本時間に変更する。
        for i in df['date']:
            i = pd.Timestamp(i).tz_convert('Asia/Tokyo')
        return df
        # csv保存はこれをプライスの下に移動
        # df = df.set_index('date')
        # df.index = df.index.astype('datetime64')
        # df.to_csv('data/USD_JPY/' + get_date_csv + '_' + end_csv + '.csv', encoding='UTF8')

# CSVファイルを保存するフォルダーを自動作成
    def create_folder(self, ex_pair, span, year):
        new_dir_path_recursive = 'data/' + ex_pair + '/' + year + '/' + span
        try:
            os.makedirs(new_dir_path_recursive)
        except FileExistsError:
            pass

# 文字列を時間に変更
    def string_to_datetime(self, string):
        return datetime.strptime(string, "%Y-%m-%d %H:%M:%S")

# oandaからデータを取得しCSVファイルに保存
    def get_csv(self, end, get_date, year, pair, span):

        ex_pair = pair  # "USD_JPY"
        asi = span  # 'M5'
        get_date_csv = get_date[:10]
        end_csv = end[:10]

        i = 0
        while(end > get_date):
            params = {"count": 5000, "granularity": asi, "to": end}
            r = instruments.InstrumentsCandles(
                instrument=ex_pair, params=params,)
            apires = self.oanda.request(r)
            res = r.response['candles']
            end = res[0]['time']
            print(res[-1]['time'])
            print(ex_pair)
            n = 0
            if i == 0:
                res1 = res
            else:
                for j in range(n, len(res1)):
                    res.append(res1[j])
                if end < get_date:
                    for j in range(5000):
                        if res[j]['time'] > get_date:
                            end = res[j - 1]['time']
                            n = j
                            break
            res1 = res[n:]
            # if i % 100 == 0:
            #     print('res ok', i + 1, 'and', 'time =', res1[0]['time'])
            i += 1

        # print('GET Finish!', i * 5000 - n)  # どのくらいデータを取得したか確認

        data = []
        # 少し形を成形してあげる
        for raw in res1:
            date_time = self.string_to_datetime(raw['time'][:10])
            data_start = self.string_to_datetime(get_date_csv)
            data_end = self.string_to_datetime(end_csv)

            if data_start <= date_time <= data_end:

                data.append([raw['time'], raw['mid']['o'], raw['mid']
                             ['h'], raw['mid']['l'], raw['mid']['c']])

        df = pd.DataFrame(data)
        df.columns = ['date', 'open', 'high', 'low', 'close']

        # 時間を全て日本時間に変更する。
        for i in df['date']:
            i = pd.Timestamp(i).tz_convert('Asia/Tokyo')
        # csv保存
        df = df.set_index('date')
        df.index = df.index.astype('datetime64')

        df.to_csv('data/' + ex_pair + '/' + year + '/' + asi + '/' +
                  get_date_csv + '_' + end_csv + '.csv', encoding='UTF8')
        return data

    def get_span_price(self, oanda, span, number):
        r = InstrumentsCandles(instrument="USD_JPY",
                               params={"granularity": span})
        while True:

            # APIへデータ要求
            oanda.request(r)

            # -2が終値の最新、-1は未確定
            data = r.response["candles"][number]

            # ローソク足から日時・始値・終値を取り出す
            close_time = pd.to_datetime(data["time"])  # 日時
            open_price = data["mid"]["o"]  # 始値
            high_price = data["mid"]["h"]  # 高値
            low_price = data["mid"]["l"]  # 安値
            close_price = data["mid"]["c"]  # 終値

            return {
                "時間": str(close_time),
                "始値": float(open_price),
                "高値": float(high_price),
                "安値": float(low_price),
                "終値": float(close_price)
            }

    def get_stop_limit_pips(self, data, stop_rate, limit_rate):
        stop_buy = round(data['安値'] - stop_rate, 3)  # stop buy
        stop_pip_buy = round((stop_buy - data['終値']) * 100, 3)

        limit_pip_buy = round((stop_pip_buy / -100) *
                              limit_rate, 3)  # リミット、200で二倍

        limit_buy = round(data['終値'] + limit_pip_buy / 100, 3)  # limit buy

        stop_sell = round(data['高値'] + stop_rate, 3)  # stop sell
        stop_pip_sell = round((data['終値'] - stop_sell) * 100, 3)
        limit_pip_sell = round((stop_pip_sell / -100) *
                               limit_rate, 3)  # リミット、200で二倍
        limit_sell = round(data['終値'] - limit_pip_sell / 100, 3)  # limit sell

        return {'stop_buy': str(stop_buy),
                'stop_buy_pip': str(stop_pip_buy),
                'limit_buy': str(limit_buy),
                'stop_sell': str(stop_sell),
                'stop_sell_pip': str(stop_pip_sell),
                'limit_sell': str(limit_sell),
                'now': str(data['終値'])}

    def get_balance(self):
        p = API(access_token=self.access_token)
        ret = AccountSummary(self.accountID)
        req = p.request(ret)
        balance = int(round(float(req['account']['balance'])))
        position = req['account']['openPositionCount']

        return {'balance': balance, 'position': position}

    def lot_calculation(self, balance, max_loss, stop_pip):
        try:

            lot1 = balance * max_loss / \
                float(stop_pip) / 100
        except ZeroDivisionError:
            lot1 = balance * max_loss / 1000
        if lot1 <= 0:
            lot1 = lot1 * -1
        lot2 = balance * max_loss / 1000
        if lot1 >= lot2:
            lot = math.floor(lot2)
        else:
            lot = math.floor(lot1)

        lot = str(round(lot * 10000))

        return lot

    def oder(self, stop, limit, lot):
        order_data = {"order": {
            "instrument": "USD_JPY",
            # "units": '+' + lot,
            "units": '+100000',
            "type": "MARKET",
            "stopLossOnFill": {
                "timeInForce": "GTC",
                "price": stop
            }, 'takeProfitOnFill': {
                'timeInForce': 'GTC',
                'price': limit
            }
        }
        }

        # APIに対する注文書を作成
        o = orders.OrderCreate(self.accountID, data=order_data)

        # 注文要求
        self.oanda.request(o)

        return order_data

    def settlement(self):
        # 決済したいポジションの情報
        position_data = {"longUnits": "ALL"}

        # APIに対する注文書を作成
        p = positions.PositionClose(
            accountID=self.accountID, data=position_data, instrument="USD_JPY")

        # 注文要求
        self.oanda.request(p)


def string_to_datetime(string):
    return datetime.strptime(string, "%Y-%m-%d %H:%M:%S")


def setting_data(pair, span, spled, account_balance, year, start_month, end_month):
    ex_piar = pair
    ex_piar_cut = ex_piar[3:]
    span1 = span
    spled1 = spled  # スプレット
    account_balance1 = account_balance  # 口座残高
    year1 = year
    s_month = '-' + start_month + '_'
    e_month = '-' + end_month + '.csv'
    return ex_piar, ex_piar_cut, span1, float(spled1), int(account_balance1), year1, {s_month: e_month}


