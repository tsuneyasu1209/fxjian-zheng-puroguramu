import indicater
import get_data
import trade
import time
import os

# csvファイルに保存するスクリプト ↓は取得済み

# ex_pair_list = ['USD_JPY', 'EUR_JPY', 'GBP_JPY', 'AUD_JPY', 'NZD_JPY', 'EUR_USD', 'AUD_USD', 'GBP_USD', 'NZD_USD', 'EUR_GBP', 'AUD_CAD', 'AUD_CHF', 'AUD_NZD', 'CAD_JPY',
#                 'CHF_JPY', 'EUR_AUD', 'EUR_CAD', 'EUR_CHF', 'GBP_AUD', 'GBP_CHF', 'USD_CHF', 'USD_CAD', 'TRY_JPY', 'ZAR_JPY', 'GBP_CAD', 'GBP_NZD', 'USD_TRY', 'USD_ZAR', ]
# span_list = ['M5', 'M15', 'M30', 'H1', 'H4', 'D']
# ex_pair_list = []
# span_list = []
for ex_pair in ex_pair_list:

    for span in span_list:
        print('start get data')
        api = get_data.Api()
        data_list_year = [
            '2007',
            '2008',
            '2009',
            # '2010',
            # '2011',
            # '2012',
            # '2013',
            # '2014',
            # '2015',
            # '2016',
            # '2017',
            # '2018',
            # '2019'
        ]
        for year in data_list_year:
            print('---------------' + year + '-----------------')
            data_dict = {
                year + '-01-31T23:59:59': year + '-01-01T00:00:00',
                year + '-03-31T23:59:59': year + '-03-01T00:00:00',
                year + '-04-30T23:59:59': year + '-04-01T00:00:00',
                year + '-05-31T23:59:59': year + '-05-01T00:00:00',
                year + '-06-30T23:59:59': year + '-06-01T00:00:00',
                year + '-07-31T23:59:59': year + '-07-01T00:00:00',
                year + '-08-31T23:59:59': year + '-08-01T00:00:00',
                year + '-09-30T23:59:59': year + '-09-01T00:00:00',
                year + '-10-31T23:59:59': year + '-10-01T00:00:00',
                year + '-11-30T23:59:59': year + '-11-01T00:00:00',
                year + '-12-31T23:59:59': year + '-12-01T00:00:00'}
            if year == '2012' or year == '2016' or year == '2008':
                data_dict[year + '-02-29T23:59:59'] = year + '-02-01T00:00:00'
            else:
                data_dict[year + '-02-28T23:59:59'] = year + '-02-01T00:00:00'

            api.create_folder(ex_pair, span, year)
            for end, start in data_dict.items():
                # 一か月ベースの検証
                sum_all = api.get_csv(end, start, year, ex_pair, span)
                data = api.save(sum_all)
